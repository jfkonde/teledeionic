(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/http'), require('rxjs/add/operator/toPromise'), require('openlayers'), require('@angular/common'), require('@ng-bootstrap/ng-bootstrap'), require('ng2-charts/ng2-charts'), require('rxjs/operators'), require('@angular/material'), require('rxjs/Observable'), require('@angular/forms'), require('@angular/material/slider'), require('ngx-loading'), require('ng2-charts'), require('@angular/platform-browser/animations')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/http', 'rxjs/add/operator/toPromise', 'openlayers', '@angular/common', '@ng-bootstrap/ng-bootstrap', 'ng2-charts/ng2-charts', 'rxjs/operators', '@angular/material', 'rxjs/Observable', '@angular/forms', '@angular/material/slider', 'ngx-loading', 'ng2-charts', '@angular/platform-browser/animations'], factory) :
	(factory((global.globalcajaAngularLib = global.globalcajaAngularLib || {}),global.ng.core,global.ng.http,null,global.ol,global._angular_common,global.ng.bootstrap,global.ng2Charts_ng2Charts,global.rxjs_operators,global._angular_material,global.rxjs_Observable,global._angular_forms,global._angular_material_slider,global.ngxLoading,global.ng2Charts,global._angular_platformBrowser_animations));
}(this, (function (exports,_angular_core,_angular_http,rxjs_add_operator_toPromise,ol,_angular_common,_ngBootstrap_ngBootstrap,ng2Charts_ng2Charts,rxjs_operators,_angular_material,rxjs_Observable,_angular_forms,_angular_material_slider,ngxLoading,ng2Charts,_angular_platformBrowser_animations) { 'use strict';

var ParcelaModificada = /** @class */ (function () {
    function ParcelaModificada() {
    }
    return ParcelaModificada;
}());

var Base64Binary = /** @class */ (function () {
    function Base64Binary() {
    }
    /**
     * @param {?} b
     * @return {?}
     */
    Base64Binary.decodeArrayBuffer = function (b) {
        var /** @type {?} */ a = new ArrayBuffer(b.length / 4 * 3);
        return a = Base64Binary.decode(b, a);
    };
    /**
     * @param {?} b
     * @param {?} a
     * @return {?}
     */
    Base64Binary.decode = function (b, a) {
        var /** @type {?} */ f = Base64Binary.keyStr.indexOf(b.charAt(b.length - 1)), /** @type {?} */ n = Base64Binary.keyStr.indexOf(b.charAt(b.length - 2)), /** @type {?} */ d = b.length / 4 * 3;
        64 == f && d--;
        64 == n && d--;
        var /** @type {?} */ e, /** @type {?} */ h, /** @type {?} */ l, /** @type {?} */ m, /** @type {?} */ p = 0, /** @type {?} */ q = 0, /** @type {?} */ f2 = a ? new Array(a) : new Array(d);
        b = b.replace(/[^A-Za-z0-9\+\/\=]/g, '');
        for (p = 0; p < d; p += 3)
            e = Base64Binary.keyStr.indexOf(b.charAt(q++)),
                h = Base64Binary.keyStr.indexOf(b.charAt(q++)),
                n = Base64Binary.keyStr.indexOf(b.charAt(q++)),
                m = Base64Binary.keyStr.indexOf(b.charAt(q++)),
                e = e << 2 | h >> 4,
                h = (h & 15) << 4 | n >> 2,
                l = (n & 3) << 6 | m,
                f2[p] = e,
                64 != n && (f2[p + 1] = h),
                64 != m && (f2[p + 2] = l);
        return f2;
    };
    Base64Binary.keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    return Base64Binary;
}());

var Parcela = /** @class */ (function () {
    /**
     * @param {?} id
     * @param {?} polygon
     * @param {?} atributos
     */
    function Parcela(id, polygon, atributos) {
        this.id = id;
        this.polygon = polygon;
        this.atributos = atributos;
    }
    return Parcela;
}());

var ParcelaBox = /** @class */ (function () {
    /**
     * @param {?} id
     */
    function ParcelaBox(id) {
        this.ID = id;
    }
    return ParcelaBox;
}());

var SerializationReader = /** @class */ (function () {
    /**
     * @param {?} data
     */
    function SerializationReader(data) {
        this.utf8Decode = function (b) {
            for (var /** @type {?} */ a = '', /** @type {?} */ c = 0, /** @type {?} */ d = 0, /** @type {?} */ e = 0, /** @type {?} */ g = 0; c < b.length;)
                d = b.charCodeAt(c),
                    128 > d ? (a += String.fromCharCode(d),
                        c++) : 191 < d && 224 > d ? (e = b.charCodeAt(c + 1),
                        a += String.fromCharCode((d & 31) << 6 | e & 63),
                        c += 2) : (e = b.charCodeAt(c + 1),
                        g = b.charCodeAt(c + 2),
                        a += String.fromCharCode((d & 15) << 12 | (e & 63) << 6 | g & 63),
                        c += 3);
            return a;
        };
        this.pos = 0;
        this.f1 = Math.pow(2, 8);
        this.f2 = Math.pow(2, 16);
        this.f3 = Math.pow(2, 24);
        this.f4 = Math.pow(2, 32);
        this.f5 = Math.pow(2, 40);
        this.f6 = Math.pow(2, 48);
        this.p52 = Math.pow(2, 52);
        this.data = data;
    }
    /**
     * @return {?}
     */
    SerializationReader.prototype.ReadCoors = function () {
        for (var /** @type {?} */ b = [], /** @type {?} */ a = 0; 2 > a; a++)
            if (this.pos++,
                this.pos + 8 <= this.data.length) {
                var /** @type {?} */ c = this.data[this.pos++], /** @type {?} */ d = this.data[this.pos++], /** @type {?} */ e = this.data[this.pos++], /** @type {?} */ g = this.data[this.pos++], /** @type {?} */ h = this.data[this.pos++], /** @type {?} */ l = this.data[this.pos++], /** @type {?} */ m = this.data[this.pos++], /** @type {?} */ p = this.data[this.pos++], /** @type {?} */ q = (p & 128) >> 7, /** @type {?} */ p = (p & 127) << 4 | (m & 240) >> 4;
                if (0 === p)
                    return 0;
                if (2047 === p)
                    return 0 === q ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
                p = Math.pow(2, p - 1023 - 52);
                c = (c & 255) + (d & 255) * this.f1 + (e & 255) * this.f2 + (g & 255) * this.f3 + (h & 255) * this.f4 + (l & 255) * this.f5 + (m & 15) * this.f6 + this.p52;
                b.push(Math.pow(-1, q) * c * p);
            }
        return b;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readByte = function () {
        ////System.out.println("Estamos leyendo el byte "+this.pos);
        //System.out.println("Leemos el byte "+this.data[this.pos]);
        return this.pos < this.data.length ? this.data[this.pos++] : 0;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readObject = function () {
        //System.out.println("Leemos el tipo de objeto");
        var /** @type {?} */ b = this.readObjType();
        //System.out.println("El tipo de objeto es "+b);
        switch (b) {
            case SerializationReader.BOOLTYPE:
                return this.readBoolean();
            case SerializationReader.BYTETYPE:
                return this.readByte();
            case SerializationReader.INT16TYPE:
                return this.readInt16();
            case SerializationReader.INT32TYPE:
                return this.readInt32();
            case SerializationReader.INT64TYPE:
                return this.readInt64();
            case SerializationReader.STRINGTYPE:
                return this.readString();
            case SerializationReader.SINGLETYPE:
                return this.readSingle();
            case SerializationReader.DOUBLETYPE:
                return this.readDouble();
            case SerializationReader.DATETIMETYPE:
                return this.readDateTime();
            case SerializationReader.OTHERTYPE:
                return null;
            default:
                return null;
        }
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readDateTime = function () {
        // TODO Auto-generated method stub
        return null;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readDouble = function () {
        // TODO Auto-generated method stub
        if (this.pos + 8 <= this.data.length) {
            var /** @type {?} */ b = this.readByte(), /** @type {?} */ a = this.readByte(), /** @type {?} */ c = this.readByte(), /** @type {?} */ d = this.readByte(), /** @type {?} */ e = this.readByte(), /** @type {?} */ g = this.readByte(), /** @type {?} */ h = this.readByte(), /** @type {?} */ l = this.readByte(), /** @type {?} */ m = (l & 128) >> 7, /** @type {?} */ l = (l & 127) << 4 | (h & 240) >> 4;
            if (0 === l)
                return 0;
            if (2047 === l)
                return 0 === m ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
            l = Math.pow(2, l - 1023 - 52);
            b = (b & 255) + (a & 255) * this.f1 + (c & 255) * this.f2 + (d & 255) * this.f3 + (e & 255) * this.f4 + (g & 255) * this.f5 + (h & 15) * this.f6 + this.p52;
            return Math.pow(-1, m) * b * l;
        }
        return 0;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readBoolean = function () {
        return 0 != this.readByte();
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readInt16 = function () {
        if (this.pos + 2 <= this.data.length) {
            var /** @type {?} */ b = this.readByte();
            return (this.readByte() & 255) << 8 | b & 255;
        }
        return 0;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readInt32 = function () {
        if (this.pos + 4 <= this.data.length) {
            var /** @type {?} */ b = this.readByte(), /** @type {?} */ a = this.readByte(), /** @type {?} */ c = this.readByte();
            return (this.readByte() & 255) << 24 | (c & 255) << 16 | (a & 255) << 8 | b & 255;
        }
        return 0;
    };
    
    /**
     * @return {?}
     */
    SerializationReader.prototype.readInt64 = function () {
        if (this.pos + 8 <= this.data.length) {
            var /** @type {?} */ b = this.readByte(), /** @type {?} */ a = this.readByte(), /** @type {?} */ c = this.readByte(), /** @type {?} */ d = this.readByte(), /** @type {?} */ e = this.readByte(), /** @type {?} */ g = this.readByte(), /** @type {?} */ h = this.readByte();
            return (this.readByte() & 255) << 56 | (h & 255) << 48 | (g & 255) << 40 | (e & 255) << 32 | (d & 255) << 24 | (c & 255) << 16 | (a & 255) << 8 | b & 255;
        }
        return 0;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readSingle = function () {
        var /** @type {?} */ b = this.readByte(), /** @type {?} */ a = this.readByte(), /** @type {?} */ c = this.readByte(), /** @type {?} */ c = this.readByte() << 24 | c << 16 | a << 8 | b, /** @type {?} */ b = c & 2147483648 ? -1 : 1, /** @type {?} */ a = (c >> 23 & 255) - 127, /** @type {?} */ c = c & 8388607;
        if (128 == a)
            return b * (c ? Number.NaN : Number.POSITIVE_INFINITY);
        if (-127 == a) {
            if (0 == c)
                return 0 * b;
            a = -126;
            c /= 4194304;
        }
        else
            c = (c | 8388608) / 8388608;
        return b * c * Math.pow(2, a);
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readString = function () {
        if (null === this.data)
            return '';
        var /** @type {?} */ b = this.read7BitEncodedInt();
        if (0 > b || 0 === b)
            return '';
        var /** @type {?} */ a = '';
        try {
            for (var /** @type {?} */ f = this.pos; f < this.pos + b; f++)
                a += String.fromCharCode(this.data[f]);
            this.pos += b;
        }
        catch ( /** @type {?} */d) { }
        return this.utf8Decode(a);
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.read7BitEncodedInt = function () {
        var /** @type {?} */ b, /** @type {?} */ a = 0, /** @type {?} */ c = 0;
        do {
            if (35 === c)
                return -1;
            b = this.readByte();
            a |= (b & 127) << c;
            c += 7;
        } while (0 != (b & 128));
        return a;
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readObjType = function () {
        switch (this.readByte()) {
            case 0:
                return SerializationReader.NULLTYPE;
            case 1:
                return SerializationReader.BOOLTYPE;
            case 2:
                return SerializationReader.BYTETYPE;
            case 3:
                return SerializationReader.UINT16TYPE;
            case 4:
                return SerializationReader.UINT32TYPE;
            case 5:
                return SerializationReader.UINT64TYPE;
            case 6:
                return SerializationReader.SBYTETYPE;
            case 7:
                return SerializationReader.INT16TYPE;
            case 8:
                return SerializationReader.INT32TYPE;
            case 9:
                return SerializationReader.INT64TYPE;
            case 10:
                return SerializationReader.CHARTYPE;
            case 11:
                return SerializationReader.STRINGTYPE;
            case 12:
                return SerializationReader.SINGLETYPE;
            case 13:
                return SerializationReader.DOUBLETYPE;
            case 14:
                return SerializationReader.DATETIMETYPE;
            default:
                return SerializationReader.OTHERTYPE;
        }
    };
    /**
     * @return {?}
     */
    SerializationReader.prototype.readNCoors = function () {
        var /** @type {?} */ b = [];
        this.pos++;
        for (var /** @type {?} */ a = this.data[this.pos++], /** @type {?} */ c = this.data[this.pos++], /** @type {?} */ d = this.data[this.pos++], /** @type {?} */ a = (this.data[this.pos++] & 255) << 24 | (d & 255) << 16 | (c & 255) << 8 | a & 255, /** @type {?} */ c = 0; c < 2 * a; c++)
            if (this.pos++,
                this.pos + 8 <= this.data.length) {
                var /** @type {?} */ e = this.data[this.pos++], /** @type {?} */ g = this.data[this.pos++], /** @type {?} */ h = this.data[this.pos++], /** @type {?} */ l = this.data[this.pos++], /** @type {?} */ m = this.data[this.pos++], /** @type {?} */ p = this.data[this.pos++], /** @type {?} */ q = this.data[this.pos++], /** @type {?} */ r = this.data[this.pos++], /** @type {?} */ d = (r & 128) >> 7, /** @type {?} */ r = (r & 127) << 4 | (q & 240) >> 4;
                0 === r ? d = 0 : 2047 === r ? d = 0 === d ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY : (r = Math.pow(2, r - 1023 - 52),
                    e = (e & 255) + (g & 255) * this.f1 + (h & 255) * this.f2 + (l & 255) * this.f3 + (m & 255) * this.f4 + (p & 255) * this.f5 + (q & 15) * this.f6 + this.p52,
                    d = Math.pow(-1, d) * e * r);
                b.push(d);
            }
        return b;
    };
    SerializationReader.NULLTYPE = 0;
    SerializationReader.BOOLTYPE = 1;
    SerializationReader.BYTETYPE = 2;
    SerializationReader.UINT16TYPE = 3;
    SerializationReader.UINT32TYPE = 4;
    SerializationReader.UINT64TYPE = 5;
    SerializationReader.SBYTETYPE = 6;
    SerializationReader.INT16TYPE = 7;
    SerializationReader.INT32TYPE = 8;
    SerializationReader.INT64TYPE = 9;
    SerializationReader.CHARTYPE = 10;
    SerializationReader.STRINGTYPE = 11;
    SerializationReader.SINGLETYPE = 12;
    SerializationReader.DOUBLETYPE = 13;
    SerializationReader.DATETIMETYPE = 14;
    SerializationReader.OTHERTYPE = 15;
    return SerializationReader;
}());

var BinaryGeometrySerializer = /** @class */ (function () {
    /**
     * @param {?} data
     */
    function BinaryGeometrySerializer(data) {
        this.atributenames = ['PROVINCIA', 'MUNICIPIO', 'AGREGADO', 'ZONA', 'POLIGONO', 'PARCELA', 'DN_SURFACE'];
        this.data = data;
    }
    /**
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.read = function () {
        var /** @type {?} */ resultado = new Array();
        var /** @type {?} */ reader = new SerializationReader(this.data);
        var /** @type {?} */ b = new SerializationReader(this.data);
        if (0 === b.readObject())
            for (var /** @type {?} */ f = b.readObject(), /** @type {?} */ d = 0; d < f; d++)
                resultado.push(this.ReadEntity(b));
        return resultado;
    };
    /**
     * @param {?} s
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadEntity = function (s) {
        var /** @type {?} */ b = s.readObject();
        var /** @type {?} */ id = s.readObject();
        var /** @type {?} */ ncoords = s.readObject();
        var /** @type {?} */ coords = new Array(ncoords);
        for (var /** @type {?} */ n = 0; n < ncoords; n++) {
            coords[n] = s.readObject();
        }
        var /** @type {?} */ pb = new ParcelaBox(id);
        pb.coords = coords;
        return pb;
    };
    /**
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadFeatures = function () {
        var /** @type {?} */ f = [], /** @type {?} */ d = new SerializationReader(this.data);
        if (0 === d.readObject())
            for (var /** @type {?} */ k = d.readObject(), /** @type {?} */ g = 0; g < k; g++)
                f.push(this.ReadFeatureVector(d, this.atributenames));
        return f;
    };
    /**
     * @param {?} b
     * @param {?} a
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadFeatureVector = function (b, a) {
        var /** @type {?} */ f = b.readObject(), /** @type {?} */ d = '' + b.readObject();
        0 != f && b.readObject();
        var /** @type {?} */ e = null;
        switch (f) {
            case 1:
                e = this.ReadPoint(b);
                break;
            case 2:
                e = this.ReadLineString(b);
                break;
            case 3:
                e = this.ReadPolygon(b);
                break;
            case 4:
                e = this.ReadMultiPoint(b);
                break;
            case 5:
                e = this.ReadMultiLineString(b);
                break;
            case 6:
                e = this.ReadMultiPolygon(b);
        }
        f = this.ReadOpenLayersAttributes(b, a);
        var /** @type {?} */ parcela = new Parcela(d, e, f);
        return parcela;
    };
    /**
     * @param {?} b
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadPoint = function (b) {
        return null;
    };
    /**
     * @param {?} b
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadMultiPoint = function (b) {
        return null;
    };
    /**
     * @param {?} b
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadMultiLineString = function (b) {
        return null;
    };
    /**
     * @param {?} b
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadMultiPolygon = function (b) {
        return null;
    };
    /**
     * @param {?} s
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadPoints = function (s) {
        var /** @type {?} */ a = [];
        var /** @type {?} */ b = s.readNCoors();
        for (var /** @type {?} */ c = 0, /** @type {?} */ d = 0; d < b.length; d += 2) {
            var /** @type {?} */ e = b[c++], /** @type {?} */ g = b[c++];
            a.push([e, g]);
        }
        return a;
    };
    /**
     * @param {?} s
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadPolygon = function (s) {
        for (var /** @type {?} */ a = [], /** @type {?} */ f = s.readObject(), /** @type {?} */ d = 0; d < f; d++) {
            var /** @type {?} */ e = this.ReadRing(s);
            null != e && a.push(e);
        }
        return a;
    };
    /**
     * @param {?} s
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadRing = function (s) {
        var /** @type {?} */ b = this.ReadPoints(s);
        return b;
    };
    /**
     * @param {?} s
     * @param {?} a
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadOpenLayersAttributes = function (s, a) {
        for (var /** @type {?} */ f = this.ReadEntityAttributes(s), /** @type {?} */ d = {}, /** @type {?} */ e = 0; e < a.length; e++) {
            var /** @type {?} */ g = null;
            e < f.length && (g = f[e]);
            d[a[e]] = g;
        }
        return d;
    };
    /**
     * @param {?} s
     * @return {?}
     */
    BinaryGeometrySerializer.prototype.ReadEntityAttributes = function (s) {
        for (var /** @type {?} */ a = s.readObject(), /** @type {?} */ c = [], /** @type {?} */ d = 0; d < a; d++)
            c[d] = s.readObject();
        return c;
    };
    return BinaryGeometrySerializer;
}());

var GeoTileBounds = /** @class */ (function () {
    /**
     * @param {?} xMax
     * @param {?} xMin
     * @param {?} yMax
     * @param {?} yMin
     * @param {?} srid
     */
    function GeoTileBounds(xMax, xMin, yMax, yMin, srid) {
        this.XMax = xMax;
        this.XMin = xMin;
        this.YMax = yMax;
        this.YMin = yMin;
        this.srid = srid;
    }
    return GeoTileBounds;
}());

var GeoTileSystemParams = /** @class */ (function () {
    /**
     * @param {?} a
     * @param {?} b
     * @param {?} c
     * @param {?} d
     */
    function GeoTileSystemParams(a, b, c, d) {
        this.XMax = a;
        this.XMin = b;
        this.YMax = c;
        this.YMin = d;
    }
    return GeoTileSystemParams;
}());

var GeoTileSystemTileIndex = /** @class */ (function () {
    /**
     * @param {?} level
     * @param {?} i
     * @param {?} j
     */
    function GeoTileSystemTileIndex(level, i, j) {
        this.i = i;
        this.j = j;
        this.level = level;
    }
    return GeoTileSystemTileIndex;
}());

var GeoTileSystem = /** @class */ (function () {
    /**
     * @param {?} a
     */
    function GeoTileSystem(a) {
        this.srid = a;
        this.ny0 = this.nx0 = 1;
        this.epsilon = 1E-10;
        this.pars = this.GetSridParams(a);
        if (null == this.pars)
            throw Error('GeoTileSystem con SRID = ' + a + ' no soportado');
        this.LoadPixelsPerVectorUnit(a);
    }
    /**
     * @param {?} a
     * @return {?}
     */
    GeoTileSystem.prototype.GetSridParams = function (a) {
        return new GeoTileSystemParams(2.00375083427892E7, -2.00375083427892E7, 2.00375083427892E7, -2.00375083427892E7);
    };
    /**
     * @param {?} b
     * @return {?}
     */
    GeoTileSystem.prototype.LoadPixelsPerVectorUnit = function (b) {
    };
    /**
     * @param {?} a
     * @param {?} b
     * @param {?} c
     * @return {?}
     */
    GeoTileSystem.prototype.XYToTileIJ = function (a, b, c) {
        var /** @type {?} */ e = Math.pow(2, c);
        a = parseInt(((a - this.pars.XMin) / ((this.pars.XMax - this.pars.XMin) / (e * this.nx0))).toString());
        b = parseInt(((b - this.pars.YMin) / ((this.pars.YMax - this.pars.YMin) / (e * this.ny0))).toString());
        return new GeoTileSystemTileIndex(c, a, b);
    };
    return GeoTileSystem;
}());

var VectorSource = /** @class */ (function () {
    function VectorSource() {
    }
    /**
     * @param {?} b
     * @param {?} srid
     * @return {?}
     */
    VectorSource.getURLTiles = function (b, srid) {
        var /** @type {?} */ r = new Array();
        var /** @type {?} */ gts = new GeoTileSystem(srid);
        var /** @type {?} */ min = gts.XYToTileIJ(b.XMin, b.YMin, 15);
        var /** @type {?} */ max = gts.XYToTileIJ(b.XMax, b.YMax, 15);
        for (var /** @type {?} */ h = min.i; h <= max.i; h++) {
            for (var /** @type {?} */ p = min.j; p <= max.j; p++) {
                var /** @type {?} */ url = '@' + srid + '/15.' + h + '.' + p;
                r.push(url);
            }
        }
        return r;
    };
    return VectorSource;
}());

var FegarequestsService = /** @class */ (function () {
    /**
     * @param {?} http
     */
    function FegarequestsService(http) {
        this.http = http;
    }
    /**
     * @param {?} coords1
     * @param {?} coords2
     * @param {?} recintos
     * @return {?}
     */
    FegarequestsService.prototype.getParcelas = function (coords1, coords2, recintos) {
        var _this = this;
        var /** @type {?} */ gtb = new GeoTileBounds(coords2[0], coords1[0], coords2[1], coords1[1], 3857);
        return new Promise(function (resolve, reject) {
            var /** @type {?} */ urls = VectorSource.getURLTiles(gtb, 3857);
            //console.log("Hemos obtenido las urls");
            //console.log(urls);
            var /** @type {?} */ parcelas = new Array();
            var /** @type {?} */ tipo = 'RECINTO';
            if (!recintos) {
                tipo = 'PARCELA';
            }
            var /** @type {?} */ nr = 0;
            for (var /** @type {?} */ n = 0; n < urls.length; n++) {
                var /** @type {?} */ urlv = FegarequestsService.URL_BASE + 'vector/' + tipo + urls[n] + '.gzip';
                _this.http.get(urlv).subscribe(function (response) {
                    var /** @type {?} */ r = response.text('legacy');
                    var /** @type {?} */ content = Base64Binary.decodeArrayBuffer(r);
                    var /** @type {?} */ bgs = new BinaryGeometrySerializer(content);
                    var /** @type {?} */ parclist = bgs.ReadFeatures();
                    for (var /** @type {?} */ p = 0; p < parclist.length; p++) {
                        parcelas.push(parclist[p]);
                    }
                    if (nr == (urls.length - 1)) {
                        resolve(parcelas);
                    }
                    nr++;
                });
            }
        });
    };
    /**
     * @param {?} ciudad
     * @return {?}
     */
    FegarequestsService.prototype.queryLocationByAddress = function (ciudad) {
        var _this = this;
        var /** @type {?} */ url = "https://maps.googleapis.com/maps/api/geocode/json";
        return new Promise(function (resolve, reject) {
            var /** @type {?} */ options = { params: { address: ciudad } };
            _this.http.get(url, options).subscribe(function (response) {
                var /** @type {?} */ jresponse = response.json();
                if (jresponse.results.length > 0) {
                    resolve(jresponse.results[0].geometry.location);
                }
            });
        });
    };
    /**
     * @param {?} atributos
     * @param {?} recinto
     * @return {?}
     */
    FegarequestsService.prototype.getExtendedAtributos = function (atributos, recinto) {
        var _this = this;
        var /** @type {?} */ conn;
        if (recinto) {
            conn = "http://sigpac.mapama.gob.es/fega/serviciosvisorsigpac/LayerInfo.aspx?layer=recinto&id=" +
                atributos.PROVINCIA + "," +
                atributos.MUNICIPIO + "," +
                atributos.AGREGADO + "," +
                atributos.ZONA + "," +
                atributos.POLIGONO + "," +
                atributos.PARCELA + "," +
                atributos.DN_SURFACE;
        }
        else {
            conn = "http://sigpac.mapama.gob.es/fega/serviciosvisorsigpac/LayerInfo.aspx?layer=parcela&id=" +
                atributos.PROVINCIA + "," +
                atributos.MUNICIPIO + "," +
                atributos.AGREGADO + "," +
                atributos.ZONA + "," +
                atributos.POLIGONO + "," +
                atributos.PARCELA;
        }
        return new Promise(function (resolve, reject) {
            _this.http.get(conn).subscribe(function (response) {
                // console.log(response.text());
                var /** @type {?} */ el = document.createElement('html');
                el.innerHTML = response.text();
                var /** @type {?} */ colsparcela = el.querySelector('#dataGridParcela')
                    .getElementsByTagName("tr").item(1).
                    getElementsByTagName("td");
                atributos.SUPERFICIE = colsparcela.item(6).innerText;
                atributos.REFCATASTRAL = colsparcela.item(7).innerText;
                //Creamos un array de recintos
                atributos.RECINTOS = new Array();
                var /** @type {?} */ filasrecinto = el.querySelector('#dataGridRecinto')
                    .getElementsByTagName("tr");
                //Recorremos las filas de los recintos
                for (var /** @type {?} */ f = 1; f < filasrecinto.length; f++) {
                    var /** @type {?} */ recinto = {};
                    var /** @type {?} */ colsrecinto = filasrecinto.item(f).getElementsByTagName("td");
                    recinto.RECINTO = colsrecinto.item(0).innerText;
                    recinto.SUPERFICIE = colsrecinto.item(1).innerText;
                    recinto.PENDIENTE = colsrecinto.item(2).innerText;
                    recinto.USO = colsrecinto.item(3).innerText;
                    recinto.PASTOSPERCENT = colsrecinto.item(4).innerText;
                    recinto.PASTOSHA = colsrecinto.item(5).innerText;
                    recinto.COEFREGADIO = colsrecinto.item(6).innerText;
                    recinto.CODINCIDENCIA = colsrecinto.item(7).innerText;
                    recinto.REGION = colsrecinto.item(8).innerText;
                    atributos.RECINTOS.push(recinto);
                }
                resolve(atributos);
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    FegarequestsService.prototype.getParcela = function (params) {
        var _this = this;
        var /** @type {?} */ nats = params.replace(/\//g, ',').split(',').length;
        var /** @type {?} */ tipo = 'PARCELA';
        if (nats == 7) {
            tipo = 'RECINTO';
        }
        var /** @type {?} */ urlrequest = FegarequestsService.URL_BASE_QUERY + params + '.gzip';
        return new Promise(function (resolve, reject) {
            _this.http.get(urlrequest).subscribe(function (response) {
                var /** @type {?} */ r = response.text('legacy');
                var /** @type {?} */ content = Base64Binary.decodeArrayBuffer(r);
                var /** @type {?} */ bgs = new BinaryGeometrySerializer(content);
                var /** @type {?} */ pbs = bgs.read();
                if (pbs.length > 0) {
                    var /** @type {?} */ pb = pbs[0];
                    //console.log('Tenemos una parcela');
                    var /** @type {?} */ coords1 = ol.proj.transform([pb.coords[0], pb.coords[1]], 'EPSG:4326', 'EPSG:3857');
                    var /** @type {?} */ coords2 = ol.proj.transform([pb.coords[2], pb.coords[3]], 'EPSG:4326', 'EPSG:3857');
                    var /** @type {?} */ gtb = new GeoTileBounds(coords2[0], coords1[0], coords2[1], coords1[1], 3857);
                    var /** @type {?} */ urls = VectorSource.getURLTiles(gtb, 3857);
                    for (var /** @type {?} */ n = 0; n < urls.length; n++) {
                        var /** @type {?} */ urlv = FegarequestsService.URL_BASE + 'vector/' + tipo + urls[n] + '.gzip';
                        _this.http.get(urlv).subscribe(function (response) {
                            var /** @type {?} */ r = response.text('legacy');
                            var /** @type {?} */ requested = params.replace(/\//g, ',');
                            var /** @type {?} */ content = Base64Binary.decodeArrayBuffer(r);
                            var /** @type {?} */ bgs = new BinaryGeometrySerializer(content);
                            var /** @type {?} */ parclist = bgs.ReadFeatures();
                            for (var /** @type {?} */ p = 0; p < parclist.length; p++) {
                                var /** @type {?} */ parcela = parclist[p];
                                var /** @type {?} */ pattr = parcela.atributos;
                                var /** @type {?} */ pstr = pattr.PROVINCIA + ',' + pattr.MUNICIPIO + ',' + pattr.AGREGADO + ',' + pattr.ZONA + ',' + pattr.POLIGONO + ',' + pattr.PARCELA;
                                if (tipo == 'RECINTO') {
                                    pstr += ',' + pattr.DN_SURFACE;
                                }
                                if (pstr == requested) {
                                    //console.log('Resuelvo')
                                    resolve(parcela);
                                    return;
                                }
                                // console.log('Otro punto '+pstr+' VS '+requested);
                            }
                        });
                    }
                }
            });
        });
    };
    FegarequestsService.URL_BASE = 'http://aidiapp.com/proxysigpac/VectorSDG/';
    FegarequestsService.URL_BASE_QUERY = 'http://aidiapp.com/proxysigpac/VectorSDG/query/ParcelaBox/';
    FegarequestsService.decorators = [
        { type: _angular_core.Injectable },
    ];
    /**
     * @nocollapse
     */
    FegarequestsService.ctorParameters = function () { return [
        { type: _angular_http.Http, },
    ]; };
    return FegarequestsService;
}());

var OsmeditorComponent = /** @class */ (function () {
    /**
     * @param {?} service
     */
    function OsmeditorComponent(service) {
        this.service = service;
        this.modificada = new _angular_core.EventEmitter();
    }
    /**
     * @param {?} parcela
     * @return {?}
     */
    OsmeditorComponent.prototype.editarParcelaAgraria = function (parcela) {
        var _this = this;
        if (this.map != null) {
            this.editando = true;
            //BORRAMOS LO ANTERIOR
            if (this.feature != null) {
                this.source.removeFeature(this.feature);
                this.feature = null;
            }
            if (this.featureoriginal != null) {
                this.source2.removeFeature(this.featureoriginal);
                this.featureoriginal = null;
            }
            console.log("Invocado el m�todo editarParcela");
            //Vamos a llamar al servicio
            var /** @type {?} */ params = parcela.replace(/,/g, '/');
            this.service.getParcela(params).then(function (parcela) {
                console.log('He encontrado la parcela ' + parcela.id);
                var /** @type {?} */ puntos = new Array();
                for (var /** @type {?} */ i = 0; i < parcela.polygon[0].length; i++) {
                    var /** @type {?} */ coord = ol.proj.transform([parcela.polygon[0][i][0], parcela.polygon[0][i][1]], 'EPSG:3857', 'EPSG:4326');
                    puntos.push(coord);
                }
                var /** @type {?} */ multipoint = new ol.geom.LineString(puntos);
                var /** @type {?} */ multis = /** @type {?} */ (multipoint.simplify(0.00002));
                var /** @type {?} */ poly2 = new ol.geom.Polygon([multis.getCoordinates()]);
                console.log("He creado el polygon");
                // var poly2=<ol.geom.Polygon>polygon.simplify(0.00005);
                _this.feature = new ol.Feature({
                    name: "parcela",
                    geometry: poly2
                });
                _this.featureoriginal = new ol.Feature({
                    name: "parcelaorig",
                    geometry: new ol.geom.Polygon([multis.getCoordinates()])
                });
                _this.source2.addFeature(_this.featureoriginal);
                _this.source.addFeature(_this.feature);
                _this.map.getView().fit(poly2);
            });
        }
    };
    /**
     * @param {?} parcela
     * @param {?} coordenadasparcela
     * @return {?}
     */
    OsmeditorComponent.prototype.editarCoordenadasParcela = function (parcela, coordenadasparcela) {
        var _this = this;
        if (this.map != null) {
            this.editando = true;
            //BORRAMOS LO ANTERIOR
            if (this.feature != null) {
                this.source.removeFeature(this.feature);
                this.feature = null;
            }
            if (this.featureoriginal != null) {
                this.source2.removeFeature(this.featureoriginal);
                this.featureoriginal = null;
            }
            console.log("Queremos editar una parcela por coordenadas");
            var /** @type {?} */ puntos = new Array();
            for (var /** @type {?} */ i = 0; i < coordenadasparcela.length; i++) {
                var /** @type {?} */ coord = [coordenadasparcela[i][1], coordenadasparcela[i][0]];
                puntos.push(coord);
            }
            var /** @type {?} */ multipoint = new ol.geom.LineString(puntos);
            var /** @type {?} */ multis = /** @type {?} */ (multipoint.simplify(0.00002));
            var /** @type {?} */ poly2 = new ol.geom.Polygon([multis.getCoordinates()]);
            console.log("He creado el polygon");
            // var poly2=<ol.geom.Polygon>polygon.simplify(0.00005);
            this.feature = new ol.Feature({
                name: "parcela",
                geometry: poly2
            });
            this.source.addFeature(this.feature);
            this.map.getView().fit(poly2);
            //Vamos a dibujar la parcela original
            var /** @type {?} */ params = parcela.replace(/,/g, '/');
            this.service.getParcela(params).then(function (parcela) {
                console.log('He encontrado la parcela ' + parcela.id);
                var /** @type {?} */ puntos = new Array();
                for (var /** @type {?} */ i = 0; i < parcela.polygon[0].length; i++) {
                    var /** @type {?} */ coord = ol.proj.transform([parcela.polygon[0][i][0], parcela.polygon[0][i][1]], 'EPSG:3857', 'EPSG:4326');
                    puntos.push(coord);
                }
                var /** @type {?} */ multipoint = new ol.geom.LineString(puntos);
                var /** @type {?} */ multis = /** @type {?} */ (multipoint.simplify(0.00002));
                _this.featureoriginal = new ol.Feature({
                    name: "parcelaorig",
                    geometry: new ol.geom.Polygon([multis.getCoordinates()])
                });
                _this.source2.addFeature(_this.featureoriginal);
            });
        }
    };
    /**
     * @return {?}
     */
    OsmeditorComponent.prototype.ngOnInit = function () {
        if (typeof this.config === 'undefined') {
            this.config = {
                target: 'mifirstmap',
                center: [+this.centermap.split(",")[0], +this.centermap.split(",")[1]]
            };
        }
    };
    /**
     * @return {?}
     */
    OsmeditorComponent.prototype.initMap = function () {
        var _this = this;
        var /** @type {?} */ styles = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            }),
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: 'orange'
                    })
                }),
                geometry: function (feature) {
                    // return the coordinates of the first ring of the polygon
                    var /** @type {?} */ coordinates = ( /** @type {?} */(feature.getGeometry())).getCoordinates()[0];
                    return new ol.geom.MultiPoint(coordinates);
                }
            })
        ];
        var /** @type {?} */ styles2 = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'red',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.3)'
                })
            })
        ];
        // var projection = ol.proj.get('EPSG:4326');
        var /** @type {?} */ projection = ol.proj.get('EPSG:3857');
        var /** @type {?} */ projectionExtent = projection.getExtent();
        //var size = ol.extent.getWidth(projectionExtent) / 512;
        var /** @type {?} */ size = ol.extent.getWidth(projectionExtent) / 256;
        var /** @type {?} */ resolutions = new Array(18);
        var /** @type {?} */ matrixIds = new Array(18);
        for (var /** @type {?} */ z = 0; z < 18; ++z) {
            // generate resolutions and matrixIds arrays for this WMTS
            resolutions[z] = size / Math.pow(2, z);
            //matrixIds[z] = "EPSG:4326:" + z;
            matrixIds[z] = z;
        }
        var /** @type {?} */ attribution = new ol.Attribution({
            html: 'Teselas de PNOA cedido por � Instituto Geogr�fico Nacional de Espa�a'
        });
        var /** @type {?} */ features = new ol.Collection();
        var /** @type {?} */ features2 = new ol.Collection();
        this.source2 = new ol.source.Vector({ wrapX: false, features: features2 });
        this.source = new ol.source.Vector({ wrapX: false, features: features });
        this.map = new ol.Map({
            layers: [
                new ol.layer.Tile({
                    opacity: 0.7,
                    extent: projectionExtent,
                    source: new ol.source.WMTS({
                        attributions: [attribution],
                        url: 'http://www.ign.es/wmts/pnoa-ma',
                        layer: 'OI.OrthoimageCoverage',
                        // matrixSet: 'EPSG:4326',
                        matrixSet: 'EPSG:3857',
                        format: 'image/png',
                        projection: projection,
                        tileGrid: new ol.tilegrid.WMTS({
                            origin: ol.extent.getTopLeft(projectionExtent),
                            resolutions: resolutions,
                            matrixIds: matrixIds
                        }),
                        style: 'default'
                    })
                }),
                new ol.layer.Vector({
                    source: this.source2,
                    style: styles2
                }),
                new ol.layer.Vector({
                    source: this.source,
                    style: styles
                })
            ],
            target: this.config.target,
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
            }),
            view: new ol.View({
                projection: 'EPSG:4326',
                center: this.config.center,
                zoom: 15
            })
        });
        var /** @type {?} */ transofrmada = ol.proj.transform([-10.72, 36.66], 'EPSG:4326', 'EPSG:3857');
        var /** @type {?} */ modif = new ol.interaction.Modify({
            features: features,
            // the SHIFT key must be pressed to delete vertices, so
            // that new vertices can be drawn at the same position
            // of existing vertices
            deleteCondition: function (event) {
                return ol.events.condition.shiftKeyOnly(event) &&
                    ol.events.condition.singleClick(event);
            }
        });
        modif.on('modifyend', function (evt) {
            //console.log(evt);
            if (_this.trackonchange) {
                // console.log(evt);
                _this.aceptarPolygon();
            }
        });
        this.map.addInteraction(modif);
        console.log('Hemos creado el mapa');
    };
    /**
     * @return {?}
     */
    OsmeditorComponent.prototype.ngAfterViewInit = function () {
        this.initMap();
    };
    /**
     * @param {?} cambios
     * @return {?}
     */
    OsmeditorComponent.prototype.ngOnChanges = function (cambios) {
    };
    /**
     * @return {?}
     */
    OsmeditorComponent.prototype.aceptarPolygon = function () {
        console.log("Has aceptado el poligono");
        var /** @type {?} */ coordenadas = ( /** @type {?} */(this.feature.getGeometry())).getCoordinates()[0];
        for (var /** @type {?} */ c in coordenadas) {
            var /** @type {?} */ coord = coordenadas[c];
            coordenadas[c] = [coord[1], coord[0]];
        }
        var /** @type {?} */ plinestring = new ol.geom.LineString(coordenadas, ( /** @type {?} */(this.feature.getGeometry())).getLayout());
        var /** @type {?} */ respuesta = new ParcelaModificada();
        respuesta.perimetro = (Math.round(plinestring.getLength() * 10000000) / 100);
        respuesta.coordenadas = coordenadas;
        var /** @type {?} */ area = ( /** @type {?} */(this.feature.getGeometry())).getArea();
        if (area > 10000) {
            respuesta.area = (Math.round(area / 1000000 * 100) / 100);
        }
        else {
            respuesta.area = (Math.round(area * 100000000) / 100);
        }
        this.modificada.emit(respuesta);
        console.log("Hemos obtenido las coordeandas");
        console.log("Las coordenadas: " + JSON.stringify(respuesta.coordenadas));
    };
    OsmeditorComponent.decorators = [
        { type: _angular_core.Component, args: [{
                    selector: 'app-osmeditor',
                    template: "<div class=\"panel panel-default\"> <div *ngIf=\"editando\" [hidden]=\"trackonchange\" class=\"panel-heading\">Arrastra los marcadores. Para confirmar la parcela editada clica en aceptar <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"aceptarPolygon()\">Aceptar</button> </div> <div class=\"panel-body map-container\" [attr.id]=\"config.target\" ></div> </div> ",
                    styles: [" .map-container{ width:100%; height:400px; }"],
                    providers: [FegarequestsService]
                },] },
    ];
    /**
     * @nocollapse
     */
    OsmeditorComponent.ctorParameters = function () { return [
        { type: FegarequestsService, },
    ]; };
    OsmeditorComponent.propDecorators = {
        'centermap': [{ type: _angular_core.Input },],
        'config': [{ type: _angular_core.Input },],
        'trackonchange': [{ type: _angular_core.Input },],
        'modificada': [{ type: _angular_core.Output },],
    };
    return OsmeditorComponent;
}());

var OsmvisorComponent = /** @class */ (function () {
    /**
     * @param {?} service
     */
    function OsmvisorComponent(service) {
        this.service = service;
        this.seleccionada = new _angular_core.EventEmitter();
    }
    /**
     * @return {?}
     */
    OsmvisorComponent.prototype.ngOnInit = function () {
        if (typeof this.config === 'undefined') {
            this.config = {
                target: 'visormap',
                center: [+this.centermap.split(",")[0], +this.centermap.split(",")[1]]
            };
        }
    };
    /**
     * @return {?}
     */
    OsmvisorComponent.prototype.ngAfterViewInit = function () {
        console.log("El centro del mapa es " + this.config.center);
        this.initMap();
    };
    /**
     * @return {?}
     */
    OsmvisorComponent.prototype.initMap = function () {
        var _this = this;
        var /** @type {?} */ styles = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255,0)'
                })
            })
        ];
        var /** @type {?} */ styles2 = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'red',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.3)'
                })
            })
        ];
        // var projection = ol.proj.get('EPSG:4326');
        var /** @type {?} */ projection = ol.proj.get('EPSG:3857');
        var /** @type {?} */ projectionExtent = projection.getExtent();
        //var size = ol.extent.getWidth(projectionExtent) / 512;
        var /** @type {?} */ size = ol.extent.getWidth(projectionExtent) / 256;
        var /** @type {?} */ resolutions = new Array(18);
        var /** @type {?} */ matrixIds = new Array(18);
        for (var /** @type {?} */ z = 0; z < 18; ++z) {
            // generate resolutions and matrixIds arrays for this WMTS
            resolutions[z] = size / Math.pow(2, z);
            //matrixIds[z] = "EPSG:4326:" + z;
            matrixIds[z] = z;
        }
        var /** @type {?} */ attribution = new ol.Attribution({
            html: 'Teselas de PNOA cedido por � Instituto Geogr�fico Nacional de Espa�a'
        });
        var /** @type {?} */ features = new ol.Collection();
        this.source = new ol.source.Vector({ wrapX: false, features: features });
        this.map = new ol.Map({
            layers: [
                new ol.layer.Tile({
                    opacity: 0.7,
                    extent: projectionExtent,
                    source: new ol.source.WMTS({
                        attributions: [attribution],
                        url: 'http://www.ign.es/wmts/pnoa-ma',
                        layer: 'OI.OrthoimageCoverage',
                        // matrixSet: 'EPSG:4326',
                        matrixSet: 'EPSG:3857',
                        format: 'image/png',
                        projection: projection,
                        tileGrid: new ol.tilegrid.WMTS({
                            origin: ol.extent.getTopLeft(projectionExtent),
                            resolutions: resolutions,
                            matrixIds: matrixIds
                        }),
                        style: 'default'
                    })
                }),
                new ol.layer.Vector({
                    source: this.source,
                    style: styles
                })
            ],
            target: this.config.target,
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
            }),
            view: new ol.View({
                projection: 'EPSG:4326',
                center: this.config.center,
                zoom: 17
            })
        });
        var /** @type {?} */ transofrmada = ol.proj.transform([-10.72, 36.66], 'EPSG:4326', 'EPSG:3857');
        this.map.on('moveend', function (evt) {
            _this.onMoveMapa();
        });
        var /** @type {?} */ select = new ol.interaction.Select();
        this.map.addInteraction(select);
        select.on('select', function (evt) {
            _this.onSelectParcela(evt);
        });
        console.log('Hemos creado el mapa');
    };
    /**
     * @param {?} ciudad
     * @return {?}
     */
    OsmvisorComponent.prototype.buscarlocalidad = function (ciudad) {
        var _this = this;
        this.service.queryLocationByAddress(ciudad).then(function (locat) {
            _this.map.setView(new ol.View({
                projection: 'EPSG:4326',
                center: [locat.lng, locat.lat],
                zoom: 17
            }));
        });
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    OsmvisorComponent.prototype.ngOnChanges = function (changes) {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        if (changes.recintos.previousValue != changes.recintos.currentValue) {
            this.source.clear();
            if (typeof this.parcelasselected != "undefined") {
                this.parcelasselected.clear();
            }
            this.onMoveMapa();
        }
    };
    /**
     * @param {?} evt
     * @return {?}
     */
    OsmvisorComponent.prototype.onSelectParcela = function (evt) {
        var _this = this;
        this.parcelasselected = evt.target.getFeatures();
        //console.log(evt.selected[0]);
        //console.log(evt.selected[0].getProperties());
        //console.log(evt.selected[0].getProperties().name);
        //console.log(evt.selected[0].getProperties().atributos);
        this.service.getExtendedAtributos(evt.selected[0].getProperties().atributos, this.recintos).then(function (atributos) {
            //console.log("Obtenidos extended");
            //console.log(atributos);
            _this.seleccionada.emit(atributos);
        });
        //var selp=this.parcelascargadas.find(parcela => parcela.id==evt.selected.name);
        //console.log(selp.atributos);
    };
    /**
     * @return {?}
     */
    OsmvisorComponent.prototype.onMoveMapa = function () {
        var _this = this;
        //Borramos todas las parcelas
        //console.log("Se ha movido el mapa");
        var /** @type {?} */ extent$$1 = this.map.getView().calculateExtent(this.map.getSize());
        var /** @type {?} */ bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extent$$1), 'EPSG:4326', 'EPSG:3857');
        var /** @type {?} */ topRight = ol.proj.transform(ol.extent.getTopRight(extent$$1), 'EPSG:4326', 'EPSG:3857');
        var /** @type {?} */ coords1 = bottomLeft;
        var /** @type {?} */ coords2 = topRight;
        this.service.getParcelas(coords1, coords2, this.recintos).then(function (parcelas) {
            _this.parcelascargadas = parcelas;
            for (var /** @type {?} */ p in parcelas) {
                var /** @type {?} */ parcela = parcelas[p];
                var /** @type {?} */ puntos = new Array();
                for (var /** @type {?} */ i = 0; i < parcela.polygon[0].length; i++) {
                    var /** @type {?} */ coord = ol.proj.transform([parcela.polygon[0][i][0], parcela.polygon[0][i][1]], 'EPSG:3857', 'EPSG:4326');
                    puntos.push(coord);
                }
                var /** @type {?} */ multipoint = new ol.geom.LineString(puntos);
                var /** @type {?} */ multis = /** @type {?} */ (multipoint.simplify(0.00002));
                var /** @type {?} */ poly2 = new ol.geom.Polygon([multis.getCoordinates()]);
                var /** @type {?} */ feature = new ol.Feature({
                    name: parcela.id,
                    geometry: poly2,
                    atributos: parcela.atributos
                });
                _this.source.addFeature(feature);
            }
        });
    };
    OsmvisorComponent.decorators = [
        { type: _angular_core.Component, args: [{
                    selector: 'app-osmvisor',
                    template: "<div class=\"panel panel-default\"> <div class=\"panel-body map-container\" [attr.id]=\"config.target\" ></div> </div> ",
                    styles: [" .map-container{ width:100%; height:400px; }"],
                    providers: [FegarequestsService]
                },] },
    ];
    /**
     * @nocollapse
     */
    OsmvisorComponent.ctorParameters = function () { return [
        { type: FegarequestsService, },
    ]; };
    OsmvisorComponent.propDecorators = {
        'centermap': [{ type: _angular_core.Input },],
        'recintos': [{ type: _angular_core.Input },],
        'seleccionada': [{ type: _angular_core.Output },],
    };
    return OsmvisorComponent;
}());

var OsmeditorModule = /** @class */ (function () {
    function OsmeditorModule() {
    }
    OsmeditorModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    declarations: [OsmeditorComponent, OsmvisorComponent],
                    providers: [],
                    exports: [OsmeditorComponent, OsmvisorComponent],
                    imports: [ng2Charts_ng2Charts.ChartsModule, _angular_common.CommonModule, _ngBootstrap_ngBootstrap.NgbModule.forRoot()]
                },] },
    ];
    /**
     * @nocollapse
     */
    OsmeditorModule.ctorParameters = function () { return []; };
    return OsmeditorModule;
}());

var NdviData = /** @class */ (function () {
    function NdviData() {
        this.negativo = 0;
        this.muy_bajo = 0;
        this.bajo = 0;
        this.normal = 0;
        this.alto = 0;
        this.muy_alto = 0;
        this.total = 0;
        this.suma_indices = 0;
    }
    /**
     * @param {?} tipo
     * @return {?}
     */
    NdviData.prototype.getPorcentaje = function (tipo) {
        if (tipo === 'muy_bajo') {
            return this.muy_bajo / this.total;
        }
        else if (tipo === 'bajo') {
            return this.bajo / this.total;
        }
        else if (tipo === 'normal') {
            return this.normal / this.total;
        }
        else if (tipo === 'alto') {
            return this.alto / this.total;
        }
        else if (tipo === 'muy_alto') {
            return this.muy_alto / this.total;
        }
        else if (tipo === 'media') {
            return (this.suma_indices) / this.total;
        }
        else if (tipo === 'negativo') {
            return (this.negativo) / this.total;
        }
    };
    /**
     * @param {?} tipo
     * @return {?}
     */
    NdviData.prototype.incrementoUnidad = function (tipo) {
        if (tipo === 'muy_bajo') {
            this.muy_bajo += 1;
        }
        else if (tipo === 'bajo') {
            this.bajo += 1;
        }
        else if (tipo === 'normal') {
            this.normal += 1;
        }
        else if (tipo === 'alto') {
            this.alto += 1;
        }
        else if (tipo === 'muy_alto') {
            this.muy_alto += 1;
        }
        else if (tipo === 'negativo') {
            this.negativo += 1;
        }
        this.total += 1;
    };
    /**
     * @param {?} indice
     * @return {?}
     */
    NdviData.prototype.addIndiceNDVI = function (indice) {
        if (indice >= 0) {
            this.suma_indices += indice;
        }
        if (indice < 0.001) {
            this.incrementoUnidad('negativo');
            return;
        }
        else if (indice < 0.2) {
            this.incrementoUnidad('muy_bajo');
            return;
        }
        else if (indice < 0.4) {
            this.incrementoUnidad('bajo');
            return;
        }
        else if (indice < 0.6) {
            this.incrementoUnidad('normal');
            return;
        }
        else if (indice < 0.8) {
            this.incrementoUnidad('alto');
            return;
        }
        else {
            this.incrementoUnidad('muy_alto');
            return;
        }
    };
    return NdviData;
}());

/**
 * UTM zones are grouped, and assigned to one of a group of 6
 * sets.
 *
 * {int} \@private
 */
var NUM_100K_SETS = 6;
/**
 * The column letters (for easting) of the lower left value, per
 * set.
 *
 * {string} \@private
 */
var SET_ORIGIN_COLUMN_LETTERS = "AJSAJS";
/**
 * The row letters (for northing) of the lower left value, per
 * set.
 *
 * {string} \@private
 */
var SET_ORIGIN_ROW_LETTERS = "AFAFAF";
var A = 65; // A
var I = 73; // I
var O = 79; // O
var V = 86; // V
var Z = 90; // Z
/**
 * Conversion of lat/lon to MGRS.
 *
 *     WGS84 ellipsoid.
 *      100 m, 2 for 1000 m or 1 for 10000 m). Optional, default is 5.
 * @param {?} ll
 * @param {?} accuracy
 * @return {?}
 */
function forward(ll, accuracy) {
    accuracy = accuracy || 5; // default accuracy 1m
    return encode(LLtoUTM({
        lat: ll[1],
        lon: ll[0]
    }), accuracy);
}
/**
 * Conversion of MGRS to lat/lon.
 *
 *     (longitude) and top (latitude) values in WGS84, representing the
 *     bounding box for the provided MGRS reference.
 * @param {?} mgrs
 * @return {?}
 */

/**
 * @param {?} mgrs
 * @return {?}
 */

/**
 * Conversion from degrees to radians.
 *
 * @param {?} deg
 * @return {?}
 */
function degToRad(deg) {
    return deg * (Math.PI / 180.0);
}
/**
 * Converts a set of Longitude and Latitude co-ordinates to UTM
 * using the WGS84 ellipsoid.
 *
 *     representing the WGS84 coordinate to be converted.
 *     northing, zoneNumber and zoneLetter properties, and an optional
 *     accuracy property in digits. Returns null if the conversion failed.
 * @param {?} ll
 * @return {?}
 */
function LLtoUTM(ll) {
    var /** @type {?} */ Lat = ll.lat;
    var /** @type {?} */ Long = ll.lon;
    var /** @type {?} */ a = 6378137.0; //ellip.radius;
    var /** @type {?} */ eccSquared = 0.00669438; //ellip.eccsq;
    var /** @type {?} */ k0 = 0.9996;
    var /** @type {?} */ LongOrigin;
    var /** @type {?} */ eccPrimeSquared;
    var /** @type {?} */ N, /** @type {?} */ T, /** @type {?} */ C, /** @type {?} */ A, /** @type {?} */ M;
    var /** @type {?} */ LatRad = degToRad(Lat);
    var /** @type {?} */ LongRad = degToRad(Long);
    var /** @type {?} */ LongOriginRad;
    var /** @type {?} */ ZoneNumber;
    // (int)
    ZoneNumber = Math.floor((Long + 180) / 6) + 1;
    //Make sure the longitude 180.00 is in Zone 60
    if (Long === 180) {
        ZoneNumber = 60;
    }
    // Special zone for Norway
    if (Lat >= 56.0 && Lat < 64.0 && Long >= 3.0 && Long < 12.0) {
        ZoneNumber = 32;
    }
    // Special zones for Svalbard
    if (Lat >= 72.0 && Lat < 84.0) {
        if (Long >= 0.0 && Long < 9.0) {
            ZoneNumber = 31;
        }
        else if (Long >= 9.0 && Long < 21.0) {
            ZoneNumber = 33;
        }
        else if (Long >= 21.0 && Long < 33.0) {
            ZoneNumber = 35;
        }
        else if (Long >= 33.0 && Long < 42.0) {
            ZoneNumber = 37;
        }
    }
    LongOrigin = (ZoneNumber - 1) * 6 - 180 + 3; //+3 puts origin
    // in middle of
    // zone
    LongOriginRad = degToRad(LongOrigin);
    eccPrimeSquared = eccSquared / (1 - eccSquared);
    N = a / Math.sqrt(1 - eccSquared * Math.sin(LatRad) * Math.sin(LatRad));
    T = Math.tan(LatRad) * Math.tan(LatRad);
    C = eccPrimeSquared * Math.cos(LatRad) * Math.cos(LatRad);
    A = Math.cos(LatRad) * (LongRad - LongOriginRad);
    M =
        a *
            ((1 -
                eccSquared / 4 -
                3 * eccSquared * eccSquared / 64 -
                5 * eccSquared * eccSquared * eccSquared / 256) *
                LatRad -
                (3 * eccSquared / 8 +
                    3 * eccSquared * eccSquared / 32 +
                    45 * eccSquared * eccSquared * eccSquared / 1024) *
                    Math.sin(2 * LatRad) +
                (15 * eccSquared * eccSquared / 256 +
                    45 * eccSquared * eccSquared * eccSquared / 1024) *
                    Math.sin(4 * LatRad) -
                35 * eccSquared * eccSquared * eccSquared / 3072 * Math.sin(6 * LatRad));
    var /** @type {?} */ UTMEasting = k0 *
        N *
        (A +
            (1 - T + C) * A * A * A / 6.0 +
            (5 - 18 * T + T * T + 72 * C - 58 * eccPrimeSquared) *
                A *
                A *
                A *
                A *
                A /
                120.0) +
        500000.0;
    var /** @type {?} */ UTMNorthing = k0 *
        (M +
            N *
                Math.tan(LatRad) *
                (A * A / 2 +
                    (5 - T + 9 * C + 4 * C * C) * A * A * A * A / 24.0 +
                    (61 - 58 * T + T * T + 600 * C - 330 * eccPrimeSquared) *
                        A *
                        A *
                        A *
                        A *
                        A *
                        A /
                        720.0));
    if (Lat < 0.0) {
        UTMNorthing += 10000000.0; //10000000 meter offset for
        // southern hemisphere
    }
    return {
        northing: Math.trunc(UTMNorthing),
        easting: Math.trunc(UTMEasting),
        zoneNumber: ZoneNumber,
        zoneLetter: getLetterDesignator(Lat)
    };
}
/**
 * Calculates the MGRS letter designator for the given latitude.
 *
 *     for.
 * @param {?} lat
 * @return {?}
 */
function getLetterDesignator(lat) {
    //This is here as an error flag to show that the Latitude is
    //outside MGRS limits
    var /** @type {?} */ LetterDesignator = "Z";
    if (84 >= lat && lat >= 72) {
        LetterDesignator = "X";
    }
    else if (72 > lat && lat >= 64) {
        LetterDesignator = "W";
    }
    else if (64 > lat && lat >= 56) {
        LetterDesignator = "V";
    }
    else if (56 > lat && lat >= 48) {
        LetterDesignator = "U";
    }
    else if (48 > lat && lat >= 40) {
        LetterDesignator = "T";
    }
    else if (40 > lat && lat >= 32) {
        LetterDesignator = "S";
    }
    else if (32 > lat && lat >= 24) {
        LetterDesignator = "R";
    }
    else if (24 > lat && lat >= 16) {
        LetterDesignator = "Q";
    }
    else if (16 > lat && lat >= 8) {
        LetterDesignator = "P";
    }
    else if (8 > lat && lat >= 0) {
        LetterDesignator = "N";
    }
    else if (0 > lat && lat >= -8) {
        LetterDesignator = "M";
    }
    else if (-8 > lat && lat >= -16) {
        LetterDesignator = "L";
    }
    else if (-16 > lat && lat >= -24) {
        LetterDesignator = "K";
    }
    else if (-24 > lat && lat >= -32) {
        LetterDesignator = "J";
    }
    else if (-32 > lat && lat >= -40) {
        LetterDesignator = "H";
    }
    else if (-40 > lat && lat >= -48) {
        LetterDesignator = "G";
    }
    else if (-48 > lat && lat >= -56) {
        LetterDesignator = "F";
    }
    else if (-56 > lat && lat >= -64) {
        LetterDesignator = "E";
    }
    else if (-64 > lat && lat >= -72) {
        LetterDesignator = "D";
    }
    else if (-72 > lat && lat >= -80) {
        LetterDesignator = "C";
    }
    return LetterDesignator;
}
/**
 * Encodes a UTM location as MGRS string.
 *
 *     zoneLetter, zoneNumber
 * @param {?} utm
 * @param {?} accuracy
 * @return {?}
 */
function encode(utm, accuracy) {
    // prepend with leading zeroes
    var /** @type {?} */ seasting = "00000" + utm.easting, /** @type {?} */ snorthing = "00000" + utm.northing;
    return (utm.zoneNumber +
        utm.zoneLetter +
        get100kID(utm.easting, utm.northing, utm.zoneNumber) +
        seasting.substr(seasting.length - 5, accuracy) +
        snorthing.substr(snorthing.length - 5, accuracy));
}
/**
 * Get the two letter 100k designator for a given UTM easting,
 * northing and zone number value.
 *
 * @param {?} easting
 * @param {?} northing
 * @param {?} zoneNumber
 * @return {?} the two letter 100k designator for the given UTM location.
 */
function get100kID(easting, northing, zoneNumber) {
    var /** @type {?} */ setParm = get100kSetForZone(zoneNumber);
    var /** @type {?} */ setColumn = Math.floor(easting / 100000);
    var /** @type {?} */ setRow = Math.floor(northing / 100000) % 20;
    return getLetter100kID(setColumn, setRow, setParm);
}
/**
 * Given a UTM zone number, figure out the MGRS 100K set it is in.
 *
 * @param {?} i
 * @return {?}
 */
function get100kSetForZone(i) {
    var /** @type {?} */ setParm = i % NUM_100K_SETS;
    if (setParm === 0) {
        setParm = NUM_100K_SETS;
    }
    return setParm;
}
/**
 * Get the two-letter MGRS 100k designator given information
 * translated from the UTM northing, easting and zone number.
 *
 *        100k set spreadsheet, created from the UTM easting.
 *        Values are 1-8.
 *        spreadsheet, created from the UTM northing value. Values
 *        are from 0-19.
 *        spreadsheet, created from the UTM zone. Values are from
 *        1-60.
 * @param {?} column
 * @param {?} row
 * @param {?} parm
 * @return {?} two letter MGRS 100k code.
 */
function getLetter100kID(column, row, parm) {
    // colOrigin and rowOrigin are the letters at the origin of the set
    var /** @type {?} */ index = parm - 1;
    var /** @type {?} */ colOrigin = SET_ORIGIN_COLUMN_LETTERS.charCodeAt(index);
    var /** @type {?} */ rowOrigin = SET_ORIGIN_ROW_LETTERS.charCodeAt(index);
    // colInt and rowInt are the letters to build to return
    var /** @type {?} */ colInt = colOrigin + column - 1;
    var /** @type {?} */ rowInt = rowOrigin + row;
    var /** @type {?} */ rollover = false;
    if (colInt > Z) {
        colInt = colInt - Z + A - 1;
        rollover = true;
    }
    if (colInt === I ||
        (colOrigin < I && colInt > I) ||
        ((colInt > I || colOrigin < I) && rollover)) {
        colInt++;
    }
    if (colInt === O ||
        (colOrigin < O && colInt > O) ||
        ((colInt > O || colOrigin < O) && rollover)) {
        colInt++;
        if (colInt === I) {
            colInt++;
        }
    }
    if (colInt > Z) {
        colInt = colInt - Z + A - 1;
    }
    if (rowInt > V) {
        rowInt = rowInt - V + A - 1;
        rollover = true;
    }
    else {
        rollover = false;
    }
    if (rowInt === I ||
        (rowOrigin < I && rowInt > I) ||
        ((rowInt > I || rowOrigin < I) && rollover)) {
        rowInt++;
    }
    if (rowInt === O ||
        (rowOrigin < O && rowInt > O) ||
        ((rowInt > O || rowOrigin < O) && rollover)) {
        rowInt++;
        if (rowInt === I) {
            rowInt++;
        }
    }
    if (rowInt > V) {
        rowInt = rowInt - V + A - 1;
    }
    var /** @type {?} */ twoLetter = String.fromCharCode(colInt) + String.fromCharCode(rowInt);
    return twoLetter;
}

var NDVIPALETTE = [
    '110 110 110',
    '111 111 111',
    '112 112 112',
    '113 113 113',
    '114 114 114',
    '115 115 115',
    '116 116 116',
    '118 118 118',
    '119 119 119',
    '120 120 120',
    '121 121 121',
    '122 122 122',
    '123 123 123',
    '124 124 124',
    '126 126 126',
    '127 127 127',
    '128 128 128',
    '129 129 129',
    '130 130 130',
    '131 131 131',
    '132 132 132',
    '134 134 134',
    '135 135 135',
    '136 136 136',
    '137 137 137',
    '138 138 138',
    '139 139 139',
    '140 140 140',
    '142 142 142',
    '143 143 143',
    '144 144 144',
    '145 145 145',
    '146 146 146',
    '147 147 147',
    '148 148 148',
    '150 150 150',
    '151 151 151',
    '152 152 152',
    '153 153 153',
    '154 154 154',
    '155 155 155',
    '156 156 156',
    '158 158 158',
    '159 159 159',
    '160 160 160',
    '161 161 161',
    '162 162 162',
    '163 163 163',
    '164 164 164',
    '166 166 166',
    '167 167 167',
    '168 168 168',
    '169 169 169',
    '170 170 170',
    '171 171 171',
    '172 172 172',
    '174 174 174',
    '175 175 175',
    '176 176 176',
    '177 177 177',
    '178 178 178',
    '179 179 179',
    '180 180 180',
    '182 182 182',
    '183 183 183',
    '184 184 184',
    '185 185 185',
    '186 186 186',
    '187 187 187',
    '188 188 188',
    '189 189 189',
    '191 191 191',
    '192 192 192',
    '193 193 193',
    '194 194 194',
    '195 195 195',
    '196 196 196',
    '197 197 197',
    '199 199 199',
    '200 200 200',
    '201 201 201',
    '202 202 202',
    '203 203 203',
    '204 204 204',
    '205 205 205',
    '207 207 207',
    '208 208 208',
    '209 209 209',
    '210 210 210',
    '211 211 211',
    '212 212 212',
    '213 213 213',
    '215 215 215',
    '216 216 216',
    '217 217 217',
    '218 218 218',
    '219 219 219',
    '220 220 220',
    '221 221 221',
    '223 223 223',
    '224 224 224',
    '225 225 225',
    '226 226 226',
    '227 227 227',
    '228 228 228',
    '229 229 229',
    '231 231 231',
    '232 232 232',
    '233 233 233',
    '234 234 234',
    '235 235 235',
    '236 236 236',
    '237 237 237',
    '239 239 239',
    '240 240 240',
    '241 241 241',
    '242 242 242',
    '243 243 243',
    '244 244 244',
    '245 245 245',
    '247 247 247',
    '248 248 248',
    '249 249 249',
    '250 250 250',
    '251 251 251',
    '252 252 252',
    '253 253 253',
    '255 255 255',
    '255 0 0',
    '255 2 0',
    '255 5 0',
    '255 7 0',
    '255 10 0',
    '255 12 0',
    '255 15 0',
    '255 18 0',
    '255 20 0',
    '255 23 0',
    '255 25 0',
    '255 28 0',
    '255 31 0',
    '255 33 0',
    '255 36 0',
    '255 38 0',
    '255 41 0',
    '255 44 0',
    '255 46 0',
    '255 49 0',
    '255 51 0',
    '255 54 0',
    '255 57 1',
    '255 59 1',
    '255 62 1',
    '255 64 1',
    '255 67 1',
    '255 70 1',
    '255 72 1',
    '255 75 1',
    '255 77 1',
    '255 80 1',
    '255 83 1',
    '255 85 1',
    '255 88 1',
    '255 90 1',
    '255 93 1',
    '255 96 1',
    '255 98 1',
    '255 101 1',
    '255 103 1',
    '255 106 1',
    '255 109 1',
    '255 110 2',
    '255 113 2',
    '255 116 2',
    '255 119 2',
    '255 123 2',
    '255 126 2',
    '255 129 2',
    '255 133 2',
    '255 136 2',
    '255 139 2',
    '255 143 2',
    '255 146 2',
    '255 149 2',
    '255 153 2',
    '255 156 2',
    '255 159 2',
    '255 163 2',
    '255 166 2',
    '255 169 2',
    '255 173 2',
    '255 176 2',
    '255 179 2',
    '255 183 1',
    '255 186 1',
    '255 189 1',
    '255 193 1',
    '255 196 1',
    '255 199 1',
    '255 203 1',
    '255 206 1',
    '255 209 1',
    '255 213 1',
    '255 216 1',
    '255 219 1',
    '255 223 1',
    '255 226 1',
    '255 229 1',
    '255 233 1',
    '255 236 1',
    '255 239 1',
    '255 243 1',
    '255 246 1',
    '255 249 1',
    '255 251 0',
    '249 246 0',
    '243 241 0',
    '237 235 0',
    '231 230 0',
    '225 225 0',
    '219 219 0',
    '213 214 0',
    '207 209 0',
    '201 203 0',
    '195 198 0',
    '189 193 0',
    '183 187 0',
    '177 182 0',
    '171 176 0',
    '165 171 0',
    '159 166 0',
    '153 160 0',
    '147 155 0',
    '141 150 0',
    '135 144 0',
    '129 139 0',
    '123 134 0',
    '117 128 0',
    '111 123 0',
    '105 117 0',
    '99 112 0',
    '93 107 0',
    '87 101 0',
    '81 96 0',
    '75 91 0',
    '69 85 0',
    '63 80 0',
    '57 75 0',
    '51 69 0',
    '45 64 0',
    '39 58 0',
    '33 53 0',
    '27 48 0',
    '21 42 0',
    '15 37 0',
    '9 32 0'
];

//import { OsmvisorComponent } from '../osmvisor/osmvisor.component'
//import { Observable } from "rxjs/Observable";
var httpOptions = new _angular_http.RequestOptions({
    headers: new _angular_http.Headers({ 'Content-Type': 'application/json' })
});
/**
 * SentinelDataService class.
 */
var SentinelDataService = /** @class */ (function () {
    /**
     * @param {?} httpclient
     * @param {?} sentinelurl
     */
    function SentinelDataService(httpclient, sentinelurl
    //private osmvisor: OsmvisorComponent
    ) {
        this.httpclient = httpclient;
        this.sentinelurl = sentinelurl;
    }
    /**
     * @param {?} ll
     * @param {?} fecha
     * @return {?}
     */
    SentinelDataService.prototype.checkScene = function (ll, fecha) {
        var /** @type {?} */ utmstr = this.convertToMGRS(ll).substring(0, 5);
        var /** @type {?} */ year = fecha.getFullYear();
        var /** @type {?} */ week = this.getWeekNumber(fecha);
        var /** @type {?} */ urlquery = this.sentinelurl.URL_BASE_NDVI_TILER + "/checkscene/" + utmstr + "/" + year + "/" + week;
        return this.httpclient.get(urlquery).pipe(rxjs_operators.map(function (response) {
            var /** @type {?} */ resultado = response.json();
            resultado["year"] = year;
            resultado["week"] = week;
            resultado["utmstr"] = utmstr;
            return resultado;
        }));
    };
    /**
     * @param {?} utmstr
     * @return {?}
     */
    SentinelDataService.prototype.disponibilidad = function (utmstr) {
        var _this = this;
        var /** @type {?} */ urlquery = this.sentinelurl.URL_BASE_NDVI_TILER + "/disponibilidad/" + utmstr;
        return this.httpclient.get(urlquery).pipe(rxjs_operators.map(function (response) {
            var /** @type {?} */ resultado = response.json();
            var /** @type {?} */ retorno = [];
            for (var /** @type {?} */ s in resultado.semanas) {
                console.log(resultado.semanas[s]);
                var /** @type {?} */ sparts = resultado.semanas[s].split("_");
                var /** @type {?} */ fechas = _this.getDatesFromWeekNumber(Number(sparts[0]), Number(sparts[1]));
                retorno = retorno.concat(fechas);
            }
            return retorno;
        }));
    };
    /**
     * @param {?} ll
     * @return {?}
     */
    SentinelDataService.prototype.getListScene = function (ll) {
        var /** @type {?} */ utmstr = this.convertToMGRS(ll);
        var /** @type {?} */ urlquery = this.sentinelurl.URL_BASE_DATA + '?utm=' +
            utmstr.substring(0, 2) + '&lat=' + utmstr.substring(2, 3) + '&grid=' + utmstr.substring(3, 5) + '&full=true';
        if (this.cacheSentinelUrlrequest === urlquery && this.cacheSentinelScenes != null) {
            return this.cacheSentinelScenes;
        }
        this.cacheSentinelUrlrequest = urlquery;
        this.cacheSentinelScenes = this.httpclient
            .get(urlquery)
            .pipe(rxjs_operators.map(function (response) {
            var /** @type {?} */ resultado = response.json();
            return resultado.results.filter(function (scene) { return scene.coverage >= 10; });
        }), rxjs_operators.publishReplay(1), rxjs_operators.refCount());
        return this.cacheSentinelScenes;
    };
    /**
     * @param {?} request
     * @param {?} withimage
     * @return {?}
     */
    SentinelDataService.prototype.getNDVIIndexes = function (request, withimage) {
        console.log(request);
        var /** @type {?} */ tipo = withimage ? "/ambosgc" : "/indicesgc";
        return this.httpclient.post(this.sentinelurl.URL_BADE_NDVI + tipo, request, httpOptions);
    };
    /**
     * @param {?} ll
     * @return {?}
     */
    SentinelDataService.prototype.convertToMGRS = function (ll) {
        return forward(ll, 1);
    };
    /**
     * @param {?} year
     * @param {?} week
     * @return {?}
     */
    SentinelDataService.prototype.getDatesFromWeekNumber = function (year, week) {
        var /** @type {?} */ f1 = new Date();
        //Fijamos el 1 de Enero del año
        f1.setFullYear(year, 0, 1);
        //Averiguamos el dia de la semana
        var /** @type {?} */ f1ds = f1.getDay() - 1;
        //Establecemos la f1 a primer dia de la semana
        f1.setTime(f1.getTime() - (60 * 60 * 24 * f1ds * 1000));
        //Calculamos los dias según las semanas
        var /** @type {?} */ semdays = 7 * (week - 1);
        //Movemos el f1 hasta la semana correspondiente
        f1.setTime(f1.getTime() + (60 * 60 * 24 * semdays * 1000));
        //Generamos el array de retorno
        var /** @type {?} */ retorno = [];
        for (var /** @type {?} */ x = 0; x < 7; x++) {
            var /** @type {?} */ ftoadd = new Date();
            ftoadd.setTime(f1.getTime());
            retorno.push(ftoadd);
            f1.setTime(f1.getTime() + (60 * 60 * 24 * 1 * 1000));
        }
        return retorno;
    };
    /**
     * @param {?} d
     * @return {?}
     */
    SentinelDataService.prototype.getWeekNumber = function (d) {
        // Copy date so don't modify original
        d = new Date(+d);
        d.setHours(0, 0, 0, 7200000); //adelantar 2h para evitar el Math.ceil con entero exacto y compensar los GMT+2
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        // Get first day of year
        var /** @type {?} */ yearStart = new Date(d.getFullYear(), 0, 1);
        // Calculate full weeks to nearest Thursday
        var /** @type {?} */ weekNo = Math.ceil((((d.valueOf() - yearStart.valueOf()) / 86400000) + 1) / 7);
        // Return array of year and week number
        return weekNo;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    SentinelDataService.prototype.mediaNDVI = function (data) {
        var /** @type {?} */ ndvidata = new NdviData();
        for (var /** @type {?} */ n = 0; n < data.indicesNDVI.length; n++) {
            ndvidata.addIndiceNDVI(data.indicesNDVI[n]);
        }
        var /** @type {?} */ decimalpipe = new _angular_common.DecimalPipe('en');
        return Number(decimalpipe.transform(ndvidata.getPorcentaje('media'), '1.2-2'));
    };
    /**
     * @param {?} rgba
     * @return {?}
     */
    SentinelDataService.prototype.getNDVIPixelIndes = function (rgba) {
        if (rgba[0] === 0 && rgba[1] === 0 && rgba[2] === 0) {
            return null;
        }
        var /** @type {?} */ indexpix = NDVIPALETTE.findIndex(function (i) { return i === rgba[0] + " " + rgba[1] + " " + rgba[2]; });
        // console.log("INDEX: "+indexpix);
        if (indexpix >= 0) {
        }
        else {
            indexpix = this.nearestNDVIValue(rgba);
        }
        return ((indexpix - 128) / 128);
    };
    /**
     * @param {?} rgba
     * @return {?}
     */
    SentinelDataService.prototype.nearestNDVIValue = function (rgba) {
        var /** @type {?} */ distance, /** @type {?} */ minDistance = Infinity, /** @type {?} */ value;
        for (var /** @type {?} */ item in NDVIPALETTE) {
            var /** @type {?} */ ndviparts = NDVIPALETTE[item].split(' ');
            var /** @type {?} */ distance_1 = Math.sqrt(Math.pow(rgba[0] - Number(ndviparts[0]), 2) +
                Math.pow(rgba[1] - Number(ndviparts[1]), 2) +
                Math.pow(rgba[2] - Number(ndviparts[2]), 2));
            if (distance_1 < minDistance) {
                minDistance = distance_1;
                value = Number(item);
            }
        }
        return value;
    };
    /*
    loadNDVI_month(year: number, month: number): Observable<any>{ //cálculo de NDVI por mes
  
      return new Observable((observer) => {
        var ndvidata=[];
        var indice;
        let fecha = new Date();
        var sem=0;
        var suma_mes: number=0;
        var media_mes: number=0;
  
        fecha.setFullYear(year);
        fecha.setMonth(month);
        fecha.setDate(1);
  
        let first_week = this.getWeekNumber(fecha);
        fecha.setMonth(month+1); //primera semana del proximo mes (para llegar a la última semana del actual)
        let last_week = this.getWeekNumber(fecha);
  
        if(first_week>last_week){ //caso de diciembre
          last_week=52;
        }
  
        //Recorro desde primera semana del mes a la última
        for (let semana = first_week; semana<last_week; semana++){
          const sem=semana.valueOf();
          let sentinelprefix = "processing_gc/" + this.osmvisor.sentinelawsdata.utmstr + "/" + year + "/" + sem +"/";
          this.getNDVIIndexes({'coords':(this.osmvisor.currentGeometry as ol.geom.Polygon).getCoordinates()[0],
          'sceneid': OsmvisorComponent.URL_TILER_BASE+ sentinelprefix,'tilesize':256}).subscribe((r) => {
  
            if(r.indicesNDVI.length>0){ //si han índices NDVI en esa parcela y esa fecha
              ndvidata.push(this.mediaNDVI(r));
            }
            else{
              ndvidata.push(null);
            }
  
            if(ndvidata.length === (last_week-first_week)) { //si he recorrido todas las semanas del mes
              this.osmvisor.loadingndvi = false; //stop evento de loading
              ndvidata=ndvidata.filter((i) => { //función anónima o lambda para filtrar valores null del array ndvidata
                if(i!==null){
                  console.log("Valor de i en lambda: "+i)
                  return i;
                }
              });
  
              for(var i=0; i<ndvidata.length; i++){
                suma_mes+=ndvidata[i];
              }
  
              media_mes=suma_mes/ndvidata.length;
  
              let resultado = {'year':year,'month':month,'media':media_mes}; //JSON
              observer.next(resultado); //devuelvo JSON
              observer.complete(); //notificación completada
              }
          });
        }
      });
    }
    */
    SentinelDataService.decorators = [
        { type: _angular_core.Injectable },
    ];
    /**
     * @nocollapse
     */
    SentinelDataService.ctorParameters = function () { return [
        { type: _angular_http.Http, },
        { type: undefined, decorators: [{ type: _angular_core.Inject, args: ['SENTINEL_URLS',] },] },
    ]; };
    return SentinelDataService;
}());

var VisorTeledeteccionComponent = /** @class */ (function () {
    /**
     * @param {?} service
     * @param {?} sentinel
     * @param {?} zone
     * @param {?} snackBar
     * @param {?} datePipe
     * @param {?} adapter
     */
    function VisorTeledeteccionComponent(service, sentinel, zone, snackBar, datePipe, adapter) {
        var _this = this;
        this.service = service;
        this.sentinel = sentinel;
        this.zone = zone;
        this.snackBar = snackBar;
        this.datePipe = datePipe;
        this.adapter = adapter;
        this.listaescenas = [];
        this.sentinelfunction = "ndvi";
        this.fechasdisponibles = [];
        this.fechadate = new _angular_forms.FormControl(new Date().toISOString());
        this.singlefeature = false;
        this.grupopanel = 'mapa';
        //Array para guardar info de satelite en correspondencia a las graficas
        this.infosatellite = [{}, {}];
        this.seleccionada = new _angular_core.EventEmitter();
        /*Eventos para el segundo slider*/
        this.change = new _angular_core.EventEmitter(); //Evento emitido cuando el valor del slider ha cambiado.
        this.input = new _angular_core.EventEmitter(); //Evento emitido cuando se mueve el slider.
        this.filtrarfechasatelites = function (fecha) {
            console.log(_this.fechasdisponibles.length);
            return _this.fechasdisponibles.find(function (f) {
                if (f.getFullYear() === fecha.getFullYear() && f.getMonth() === fecha.getMonth()
                    && f.getDate() === fecha.getDate()) {
                    return true;
                }
                else {
                    return false;
                }
            });
        };
    }
    
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.ngOnInit = function () {
        this.adapter.setLocale('es'); //establecer la fecha editable en formato dd/MM/YYYY
        console.log("Vamos a mostrar las fechas");
        // this.adapter.setLocale('es');
        this.fourweeks = {
            lineChartLabels: ['1', '2', '3', '4'],
            lineChartColors: [
                {
                    backgroundColor: '#aac08c',
                    borderColor: '#6f9154',
                    pointBackgroundColor: '#6f9154',
                    pointBorderColor: '#3d5521',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#6f9154'
                }, {
                    backgroundColor: '#b3b3b3',
                    borderColor: '#808080',
                    pointBackgroundColor: '#808080',
                    pointBorderColor: '#b3b3b3',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#808080'
                }
            ],
            lineChartOptions: {
                scales: {
                    yAxes: [{
                            ticks: {
                                steps: 10,
                                stepValue: 0.1,
                                max: 1,
                                min: 0,
                            }
                        }]
                },
                hover: {
                    mode: "nearest",
                    intersec: true,
                },
                interaction: {
                    mode: "nearest",
                }
            }
        };
        this.fourweeks_year = {
            lineChartLabels: ['1', '2', '3', '4'],
            lineChartColors: [
                {
                    backgroundColor: '#aac08c',
                    borderColor: '#6f9154',
                    pointBackgroundColor: '#6f9154',
                    pointBorderColor: '#3d5521',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#6f9154'
                }
            ],
            lineChartOptions: {
                scales: {
                    yAxes: [{
                            ticks: {
                                steps: 10,
                                stepValue: 0.1,
                                max: 1,
                                min: 0,
                            }
                        }]
                }
            }
        };
        this.threemonths = {
            lineChartLabels: ['1', '2', '3'],
            lineChartColors: [
                {
                    backgroundColor: '#aac08c',
                    borderColor: '#6f9154',
                    pointBackgroundColor: '#6f9154',
                    pointBorderColor: '#3d5521',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#6f9154'
                }, {
                    backgroundColor: '#b3b3b3',
                    borderColor: '#808080',
                    pointBackgroundColor: '#808080',
                    pointBorderColor: '#b3b3b3',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#808080'
                }
            ],
            lineChartOptions: {
                scales: {
                    yAxes: [{
                            ticks: {
                                steps: 10,
                                stepValue: 0.1,
                                max: 1,
                                min: 0,
                            }
                        }]
                },
                hover: {
                    mode: "nearest",
                    intersec: true,
                },
                interaction: {
                    mode: "nearest",
                }
            }
        };
        this.threemonths_year = {
            lineChartLabels: ['1', '2', '3'],
            lineChartColors: [
                {
                    backgroundColor: '#aac08c',
                    borderColor: '#6f9154',
                    pointBackgroundColor: '#6f9154',
                    pointBorderColor: '#3d5521',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#6f9154'
                }
            ],
            lineChartOptions: {
                scales: {
                    yAxes: [{
                            ticks: {
                                steps: 10,
                                stepValue: 0.1,
                                max: 1,
                                min: 0,
                            }
                        }]
                }
            }
        };
        //En realidad no son 4 gráficas como tal, sino 2 con 2 representaciones gráficas cada una. (1º-2º)=primera, (3º-4º)=segunda
        if (typeof this.config === "undefined") {
            this.config = {
                target: "visormap",
                center: [+this.centermap.split(",")[0], +this.centermap.split(",")[1]]
            };
        }
    };
    /**
     * @param {?} obj
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.getElementPosition = function (obj) {
        var /** @type {?} */ curleft = 0, /** @type {?} */ curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return { x: curleft, y: curtop };
        }
        return undefined;
    };
    /**
     * @param {?} element
     * @param {?} event
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.getEventLocation = function (element, event) {
        // Relies on the getElementPosition function.
        var /** @type {?} */ pos = this.getElementPosition(element);
        return {
            x: (event.pageX - pos.x),
            y: (event.pageY - pos.y)
        };
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.drawImageOnCanvas = function () {
        var _this = this;
        if (this.canvasparcela != null) {
            setTimeout(function () {
                var /** @type {?} */ canvas = /** @type {?} */ (_this.canvasparcela.nativeElement);
                var /** @type {?} */ canvascontainer = /** @type {?} */ (_this.canvascontainer.nativeElement);
                var /** @type {?} */ cw = canvas.offsetWidth;
                var /** @type {?} */ iw = _this.detailedimage.width;
                var /** @type {?} */ rw = iw / cw;
                console.log("La ratio es " + rw);
                if (iw < cw) {
                    //La imagen es mas pequeña
                    console.log("La imagen es más pequeña que el canvas");
                    var /** @type {?} */ nw = /** @type {?} */ void 0, /** @type {?} */ nh = void 0;
                    if (rw < 0.25) {
                        nw = (iw * 4);
                        nh = (_this.detailedimage.height * 4);
                    }
                    else {
                        nw = iw / rw;
                        nh = _this.detailedimage.height / rw;
                    }
                    canvas.width = nw;
                    canvas.height = nh;
                    canvas.style.width = nw + "px";
                    canvas.style.height = nh + "px";
                    canvascontainer.style.width = nw + "px";
                    canvascontainer.style.height = nh + "px";
                }
                else if (iw > cw) {
                    //La imagen es más grande
                    var /** @type {?} */ nw = canvas.width / rw;
                    var /** @type {?} */ nh = canvas.height / rw;
                    canvas.width = nw;
                    canvas.height = nh;
                    canvas.style.width = nw + "px";
                    canvas.style.height = nh + "px";
                    canvascontainer.style.width = nw + "px";
                    canvascontainer.style.height = nh + "px";
                }
                console.log("El ancho del canvas es " + cw);
                canvas.getContext("2d").drawImage(_this.detailedimage, 0, 0, _this.detailedimage.width, _this.detailedimage.height, 0, 0, canvas.width, canvas.height);
                if (_this.pixelndvidata != null) {
                    /*Actualizar valor de Indice Seleccionado*/
                    console.log("Actualizar indice seleccionado");
                    var /** @type {?} */ pixelData = _this.context.getImageData(_this.eventLocation.x, _this.eventLocation.y, 1, 1).data;
                    console.log("RGBA => ", pixelData);
                    var /** @type {?} */ indexp = _this.sentinel.getNDVIPixelIndes(pixelData);
                    console.log(indexp);
                    // Convert it to HEX if you want using the rgbToHex method.
                    var /** @type {?} */ hex = "#" + ("000000" + _this.rgbToHex(pixelData[0], pixelData[1], pixelData[2])).slice(-6);
                    console.log(hex);
                    _this.pixelndvidata = { 'ndvi': indexp, 'color': hex, 'loc': _this.eventLocation };
                    console.log(_this.pixelndvidata);
                    if (indexp == null) {
                        _this.pixelndvidata = null;
                    }
                    console.log(_this.pixelndvidata);
                }
            }, 250);
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.picarPixel = function (e) {
        var /** @type {?} */ canvas = /** @type {?} */ (this.canvasparcela.nativeElement);
        var /** @type {?} */ cw = canvas.width;
        console.log("El ancho del canvas es " + cw);
        console.log("Estoy en picarPixel()");
        // Get the coordinates of the click
        this.eventLocation = this.getEventLocation(canvas, event);
        console.log(this.eventLocation);
        // Get the data of the pixel according to the location generate by the getEventLocation function
        this.context = canvas.getContext('2d');
        var /** @type {?} */ pixelData = this.context.getImageData(this.eventLocation.x, this.eventLocation.y, 1, 1).data;
        console.log("RGBA => ", pixelData);
        var /** @type {?} */ indexp = this.sentinel.getNDVIPixelIndes(pixelData);
        // If transparency on the pixel , array = [0,0,0,0]
        if ((pixelData[0] == 0) && (pixelData[1] == 0) && (pixelData[2] == 0) && (pixelData[3] == 0)) {
            // Do something if the pixel is transparent
        }
        // Convert it to HEX if you want using the rgbToHex method.
        var /** @type {?} */ hex = "#" + ("000000" + this.rgbToHex(pixelData[0], pixelData[1], pixelData[2])).slice(-6);
        console.log(hex);
        this.pixelndvidata = { 'ndvi': indexp, 'color': hex, 'loc': this.eventLocation };
        console.log(this.pixelndvidata);
        if (indexp == null) {
            this.pixelndvidata = null;
        }
        console.log(this.pixelndvidata);
    };
    /**
     * @param {?} r
     * @param {?} g
     * @param {?} b
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.rgbToHex = function (r, g, b) {
        if (r > 255 || g > 255 || b > 255)
            throw "Invalid color component";
        return ((r << 16) | (g << 8) | b).toString(16);
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.ngAfterViewInit = function () {
        console.log("El centro del mapa es " + this.config.center);
        this.initMap(); //Inicializo el mapa
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.refreshMapa = function () {
        this.source.changed();
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.initMap = function () {
        var _this = this;
        var /** @type {?} */ styles = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: "blue",
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: "rgba(0, 0, 255,0)"
                })
            })
        ];
        var /** @type {?} */ selStyle = [
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: "black",
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: "rgba(0, 0, 255,0)"
                })
            })
        ];
        var /** @type {?} */ styles2 = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: "red",
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: "rgba(255, 255, 255, 0.3)"
                })
            })
        ];
        // var projection = ol.proj.get('EPSG:4326');
        var /** @type {?} */ projection = ol.proj.get("EPSG:3857");
        var /** @type {?} */ projectionExtent = projection.getExtent();
        //var size = ol.extent.getWidth(projectionExtent) / 512;
        var /** @type {?} */ size = ol.extent.getWidth(projectionExtent) / 256;
        var /** @type {?} */ resolutions = new Array(18);
        var /** @type {?} */ matrixIds = new Array(18);
        for (var /** @type {?} */ z = 0; z < 18; ++z) {
            // generate resolutions and matrixIds arrays for this WMTS
            resolutions[z] = size / Math.pow(2, z);
            //matrixIds[z] = "EPSG:4326:" + z;
            matrixIds[z] = z;
        }
        var /** @type {?} */ attribution = new ol.Attribution({
            html: "Teselas de PNOA cedido por � Instituto Geogr�fico Nacional de Espa�a"
        });
        var /** @type {?} */ features = new ol.Collection();
        this.source = new ol.source.Vector({ wrapX: false, features: features });
        this.map = new ol.Map({
            layers: [
                new ol.layer.Tile({
                    zIndex: 0,
                    opacity: 1,
                    extent: projectionExtent,
                    source: new ol.source.WMTS({
                        attributions: [attribution],
                        url: "http://www.ign.es/wmts/pnoa-ma",
                        layer: "OI.OrthoimageCoverage",
                        // matrixSet: 'EPSG:4326',
                        matrixSet: "EPSG:3857",
                        format: "image/png",
                        projection: projection,
                        tileGrid: new ol.tilegrid.WMTS({
                            origin: ol.extent.getTopLeft(projectionExtent),
                            resolutions: resolutions,
                            matrixIds: matrixIds
                        }),
                        style: "default"
                    })
                }),
                new ol.layer.Vector({
                    zIndex: 5,
                    source: this.source,
                    style: styles
                })
            ],
            target: this.config.target,
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
            }),
            view: new ol.View({
                projection: "EPSG:4326",
                center: this.config.center,
                zoom: 16
            })
        });
        var /** @type {?} */ transofrmada = ol.proj.transform([-10.72, 36.66], "EPSG:4326", "EPSG:3857");
        this.map.on("moveend", function (evt) {
            _this.onMoveMapa();
        });
        this.select = new ol.interaction.Select();
        this.map.addInteraction(this.select);
        this.select.on("select", function (evt) {
            var /** @type {?} */ selected = evt.selected;
            var /** @type {?} */ deselected = evt.deselected;
            if (selected.length) {
                selected.forEach(function (feature) {
                    feature.setStyle(selStyle);
                });
            }
            deselected.forEach(function (feature) {
                feature.setStyle(styles);
            });
            _this.onSelectParcelaOnMapa(evt);
        });
        console.log("Hemos creado el mapa");
    };
    /**
     * @param {?} ciudad
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.buscarlocalidad = function (ciudad) {
        /*
        this.service.queryLocationByAddress(ciudad).then((locat) => {
    
          this.map.setView(new ol.View({
            projection: 'EPSG:4326',
            center: [locat.lng,locat.lat],
            zoom: 17
          }));
        });
        */
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.ngOnChanges = function (changes) {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        if (changes.recintos.previousValue != changes.recintos.currentValue) {
            if (typeof this.source != "undefined") {
                this.source.clear();
            }
            if (typeof this.parcelasselected != "undefined") {
                this.parcelasselected.clear();
                this.onMoveMapa();
            }
        }
    };
    /**
     * @param {?} feature
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.onSelectParcela = function (feature) {
        var _this = this;
        this.currentGeometry = feature.getGeometry();
        var /** @type {?} */ sigpacatts = feature.getProperties().atributos;
        this.currentsigpacid = sigpacatts.PROVINCIA + "-" + sigpacatts.MUNICIPIO +
            "-" + sigpacatts.AGREGADO +
            "-" + sigpacatts.ZONA +
            "-" + sigpacatts.POLIGONO +
            "-" + sigpacatts.PARCELA +
            "-" + sigpacatts.DN_SURFACE;
        this.grupopanel = 'indices';
        this.loadNDVI();
        this.loadNDVI_year();
        this.loadNDVI_threemonths();
        //this.loadNDVI_threemonths_year();
        this.checkEscenaAndCrop();
        //console.log(evt.selected[0]);
        //console.log(evt.selected[0].getProperties().name);
        console.log(feature.getProperties().atributos);
        this.service
            .getExtendedAtributos(feature.getProperties().atributos, this.recintos)
            .then(function (atributos) {
            //console.log("Obtenidos extended");
            //console.log(atributos);
            _this.seleccionada.emit(atributos);
        });
        //var selp=this.parcelascargadas.find(parcela => parcela.id==evt.selected.name);
        //console.log(selp.atributos);
    };
    /**
     * @param {?} evt
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.onSelectParcelaOnMapa = function (evt) {
        this.parcelasselected = evt.target.getFeatures();
        console.log("Ha seleccionado una parcela ", evt.selected[0]);
        this.onSelectParcela(evt.selected[0]);
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.onMoveMapa = function () {
        //Borramos todas las parcelas
        //console.log("Se ha movido el mapa");
        //Vamos a obtener la disponibilidad de imagenes satelite
        var /** @type {?} */ extent$$1 = this.map.getView().calculateExtent(this.map.getSize());
        var /** @type {?} */ utmstr = this.sentinel.convertToMGRS(ol.extent.getBottomLeft(extent$$1)).substr(0, 5);
        if (this.utmstr !== utmstr) {
            this.utmstr = utmstr;
            this.comprobarDisponibilidadSatelite();
        }
        console.log("Zoom " + this.map.getView().getZoom());
        if (this.map.getView().getZoom() > 15) {
            this.cargarParcelarioSIGPAC();
        }
        if (this.sentinelawsprefix != null && this.sentinelawsprefix != undefined) {
            this.checkEscenaAndCrop();
        }
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.comprobarDisponibilidadSatelite = function () {
        var _this = this;
        this.sentinel.disponibilidad(this.utmstr).toPromise().then(function (r) {
            _this.fechasdisponibles = r;
            console.log("Hemos generado las fechas disponibles");
            console.log(_this.fechasdisponibles.length);
            _this.num_items_slider = _this.fechasdisponibles.length;
            _this.getCustomDates(_this.fechasdisponibles);
            console.log("Ultima fecha disponible: ", _this.localeDate_latest);
            console.log("Primera fecha disponible: ", _this.localeDate_first);
            console.log("Fechas Ordenadas: ", _this.dates_sorted.length);
            console.log(_this.dates_sorted);
            console.log(_this.sorted_ms);
            //console.log(this.fulldates);
            /*Damos valor a los inputs del slider. Empezamos en la fecha más reciente*/
            _this.slider_valueID = _this.dates_sorted.length;
            _this.slider_valueDate = _this.dates_sorted[_this.slider_valueID - 1];
        });
    };
    /**
     * @param {?} data
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.getCustomDates = function (data) {
        // Convertir a timestamp y ordenar
        this.sorted_ms = data.map(function (item) {
            return new Date(item).getTime();
        }).sort();
        // Coger la última
        var /** @type {?} */ latest_ms = this.sorted_ms[this.sorted_ms.length - 1];
        // Convertir a js date object
        this.date_latest = new Date(latest_ms);
        // Convertir a toLocaleDateString()
        this.localeDate_latest = this.date_latest.toLocaleDateString();
        // Coger la primera
        var /** @type {?} */ first_ms = this.sorted_ms[0];
        // Convertir a js date object
        this.date_first = new Date(first_ms);
        // Convertir a toLocaleDateString()
        this.localeDate_first = this.date_first.toLocaleDateString();
        // Convertir a tipo date los timestamps ya ordenados
        this.dates_sorted = this.sorted_ms.map(function (item) {
            return new Date(item).toLocaleDateString();
        });
        this.fulldates = this.sorted_ms.map(function (item) {
            return new Date(item);
        });
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.cargarParcelarioSIGPAC = function () {
        var _this = this;
        var /** @type {?} */ extent$$1 = this.map.getView().calculateExtent(this.map.getSize());
        var /** @type {?} */ bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extent$$1), "EPSG:4326", "EPSG:3857");
        var /** @type {?} */ topRight = ol.proj.transform(ol.extent.getTopRight(extent$$1), "EPSG:4326", "EPSG:3857");
        var /** @type {?} */ coords1 = bottomLeft;
        var /** @type {?} */ coords2 = topRight;
        //Comprobamos las imagenes satelite
        // this.refreshSceneList(ol.extent.getBottomLeft(extent));
        if (!this.singlefeature) {
            this.service.getParcelas(coords1, coords2, this.recintos).then(function (parcelas) {
                _this.parcelascargadas = parcelas;
                for (var /** @type {?} */ p in parcelas) {
                    var /** @type {?} */ parcela = parcelas[p];
                    var /** @type {?} */ puntos = new Array();
                    for (var /** @type {?} */ i = 0; i < parcela.polygon[0].length; i++) {
                        var /** @type {?} */ coord = ol.proj.transform([parcela.polygon[0][i][0], parcela.polygon[0][i][1]], "EPSG:3857", "EPSG:4326");
                        puntos.push(coord);
                    }
                    var /** @type {?} */ multipoint = new ol.geom.LineString(puntos);
                    var /** @type {?} */ multis = /** @type {?} */ (multipoint.simplify(0.00002));
                    var /** @type {?} */ poly2 = new ol.geom.Polygon([multis.getCoordinates()]);
                    var /** @type {?} */ feature = new ol.Feature({
                        name: parcela.id,
                        geometry: poly2,
                        atributos: parcela.atributos
                    });
                    _this.source.addFeature(feature);
                }
            });
        }
    };
    /**
     * @param {?} escena
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.loadEscena = function (escena) {
        this.currentEscena = escena;
        this.checkEscenaAndCrop();
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.checkEscenaAndCrop = function () {
        //Comprobamos si hay escena o parcela seleccionado
        var _this = this;
        /*
        if (
         // this.currentEscena === null ||
          //this.currentEscena === undefined ||
          this.currentGeometry === null ||
          this.currentGeometry === undefined
        ) {
          return;
        }
    */
        //console.log(this.currentGeometry.getExtent());
        var /** @type {?} */ projection = ol.proj.get("EPSG:3857");
        var /** @type {?} */ projectionExtent = projection.getExtent();
        console.log(projectionExtent);
        // let parcextent = this.currentGeometry.getExtent();
        // console.log(parcextent);
        var /** @type {?} */ parcextent2 = this.map.getView().calculateExtent(this.map.getSize());
        console.log(parcextent2);
        var /** @type {?} */ size = ol.extent.getWidth(projectionExtent) / 256;
        var /** @type {?} */ resolutions = new Array(15);
        var /** @type {?} */ matrixIds = new Array(15);
        for (var /** @type {?} */ z = 0; z < 15; ++z) {
            resolutions[z] = size / Math.pow(2, z);
            matrixIds[z] = z;
        }
        if (this.sentinelLayer != null || this.sentinelLayer != undefined) {
            console.log("Borramos la capa sentinel");
            this.map.removeLayer(this.sentinelLayer);
        }
        var /** @type {?} */ urltosent = this.getUrlSentinel();
        var /** @type {?} */ sourcesentinel = new ol.source.TileImage({
            url: urltosent,
            projection: ol.proj.get("EPSG:3857"),
            tileGrid: new ol.tilegrid.WMTS({
                extent: projectionExtent,
                resolutions: resolutions,
                matrixIds: matrixIds
            })
        });
        sourcesentinel.on("tileloadstart", function () {
            _this.loadingtiles = true;
        });
        sourcesentinel.on("tileloadend", function () {
            _this.loadingtiles = false;
            if (_this.sentinelfunction === "ndvi") {
                //this.loadNDVI();
            }
        });
        this.sentinelLayer = new ol.layer.Tile({
            zIndex: 2,
            opacity: 0.75,
            extent: parcextent2,
            source: sourcesentinel
        });
        this.map.addLayer(this.sentinelLayer);
        //console.log(this.parcelasselected);
        /*
        // var projection = ol.proj.get('EPSG:4326');
        var projection = ol.proj.get("EPSG:3857");
        var projectionExtent = projection.getExtent();
        //var size = ol.extent.getWidth(projectionExtent) / 512;
        var size = ol.extent.getWidth(projectionExtent) / 256;
        var resolutions = new Array(18);
        var matrixIds = new Array(18);
        for (var z = 0; z < 18; ++z) {
          // generate resolutions and matrixIds arrays for this WMTS
          resolutions[z] = size / Math.pow(2, z);
          //matrixIds[z] = "EPSG:4326:" + z;
          matrixIds[z] = z;
        }
        console.log(escena.browseURL);
    
        //Obtenemos la capa base
        let capabase: ol.layer.Layer = this.map
          .getLayers()
          .getArray()[0] as ol.layer.Layer;
        //Creamos el source
        let fuentesentinel = new ol.source.TileImage({
          url:
            OsmvisorComponent.URL_TILER_BASE +
            "tiles/" +
            this.currentEscena.scene_id +
            "/{z}/{x}/{y}.png?tile=256&rgb=04,03,02&histo=511,2761-688,2426-903,2576",
          projection: capabase.getSource().getProjection(),
          tileGrid: new ol.tilegrid.WMTS({
            origin: ol.extent.getTopLeft(projectionExtent),
            resolutions: resolutions,
            matrixIds: matrixIds
          })
        });
        capabase.setSource(fuentesentinel);
        // console.log(this.map.getLayers().getArray()[0]);
        */
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.loadNDVI = function () {
        var _this = this;
        console.log("FECHA SELECCIONADA: ", this.fechaseleccionada);
        if (this.currentGeometry == undefined || this.currentGeometry == null) {
            return;
        }
        if (this.fechaseleccionada == undefined || this.fechaseleccionada == null) {
            //Obtener la fecha disponible más reciente:
            this.fechaseleccionada = this.date_latest;
            console.log("Fecha seleccionada por defecto (la más reciente): ", this.fechaseleccionada);
            this.compruebaEscena(this.fechaseleccionada);
        }
        this.loadingndvi = true; //evento de cargando
        var /** @type {?} */ ndvidata = []; //array de índice de vegetación
        var /** @type {?} */ indice = 3;
        var /** @type {?} */ fechas = []; //array de fechas para las etiquetas
        var /** @type {?} */ fecha = new Date(this.fechaseleccionada.getTime()); //fecha del datePicker
        this.añoActual = fecha.getFullYear();
        //Reseteamos el elemento 0 del array de info salelite=
        this.infosatellite[0] = [{}, {}, {}, {}];
        for (var /** @type {?} */ f = 0; f < 4; f++) { //asignación de etiquetas
            console.log(f + " Fecha: " + fecha.toLocaleDateString()); //formato UTC con zona horaria
            fechas.push(this.datePipe.transform(fecha, 'dd/MMMM')); //añadir fecha seleccionada a array de fechas
            fecha.setTime(fecha.getTime() - (60 * 60 * 24 * 7 * 1000)); //time actual en ms - (60s 60m 24h 7d milisegundos); resto una semana
        }
        this.fourweeks.lineChartLabels = fechas.reverse(); //ordenar labels
        var _loop_1 = function () {
            sem = this_1.sentinelawsdata.week - i;
            year = this_1.sentinelawsdata.year;
            var /** @type {?} */ indi = indice.valueOf();
            if (sem <= 0) { //si semanas de año anterior
                year = year - 1; //retrocedo 1 año
                sem = 52 - sem; //retrocedo semanas desde la 52 (última de diciembre)
            }
            //crear el prefijo sentinel  (año, nºsemana)
            var /** @type {?} */ sentinelprefix = "processing_gc/" + this_1.sentinelawsdata.utmstr + "/" + year + "/" + sem + "/";
            //obtener índices NDVI con las coordenadas de parcela y el prefijo de fecha
            this_1.sentinel.getNDVIIndexes({ 'coords': ( /** @type {?} */(this_1.currentGeometry)).getCoordinates()[0],
                'sceneid': this_1.sentinel.sentinelurl.URL_BASE_NDVI_TILER + "/" + sentinelprefix, 'tilesize': (indi === 3 ? 512 : 256) }, (indi === 3 ? true : false)).subscribe(function (response) {
                var /** @type {?} */ r = response.json();
                if (r.satellite != undefined) {
                    _this.infosatellite[0][indi] = r.satellite;
                }
                if (indi == 3) {
                    _this.detailedimage = new Image();
                    _this.imagedata = r.imagedata;
                    _this.detailedimage.src = _this.imagedata;
                }
                console.log("r NDVI: " + r.indicesNDVI);
                if (r.indicesNDVI.length > 0) { //r.indicesNDVI es un array que contiene los NDVI para esa localización y fecha
                    ndvidata[indi] = _this.sentinel.mediaNDVI(r);
                }
                else {
                    ndvidata[indi] = null;
                }
                _this.fourweeks.lineChartData =
                    [{ data: ndvidata, label: 'NDVI_' + _this.añoActual },
                        { data: _this.fourweeks_year.lineChartData[0].data, label: 'NDVI_' + _this.añoAnterior }];
                if (ndvidata.length === 4) {
                    console.log("Array ndvidata: " + ndvidata);
                    //this.loadingndvi = false;
                    _this.currentndviindex = _this.fourweeks.lineChartData[0].data[3];
                    _this.currentndetaildviindex = _this.fourweeks.lineChartData[0].data[3];
                    
                    console.log("El indice medio (NDVI) de la semana actual es " + _this.currentndviindex);
                }
            });
            indice -= 1;
        };
        var this_1 = this, sem, year;
        for (var /** @type {?} */ i = 0; i < 4; i++) {
            _loop_1();
        }
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.loadNDVI_year = function () {
        var _this = this;
        console.log("FECHA SELECCIONADA: ", this.fechaseleccionada);
        if (this.currentGeometry == undefined || this.currentGeometry == null) {
            return;
        }
        if (this.fechaseleccionada == undefined || this.fechaseleccionada == null) {
            //Obtener la fecha disponible más reciente:
            this.fechaseleccionada = this.date_latest;
            console.log("Fecha seleccionada por defecto (la más reciente): ", this.fechaseleccionada);
            this.compruebaEscena(this.fechaseleccionada);
        }
        this.loadingndvi = true; //evento de cargando
        var /** @type {?} */ ndvidata = []; //array de índice de gegetación
        var /** @type {?} */ indice = 3;
        var /** @type {?} */ fechas = []; //array de fechas para las etiquetas
        var /** @type {?} */ fecha = new Date(this.fechaseleccionada.getTime()); //fecha del datePicker;
        fecha.setTime(fecha.getTime() - (60 * 60 * 24 * 7 * 52 * 1000)); //establezco esta fecha hace un año
        this.añoAnterior = fecha.getFullYear();
        //Reseteamos el elemento 0 del array de info salelite=
        this.infosatellite[1] = [{}, {}, {}, {}];
        for (var /** @type {?} */ f = 0; f < 4; f++) {
            console.log(f + " Fecha: " + fecha.toLocaleDateString()); //formato UTC con zona horaria
            fechas.push(this.datePipe.transform(fecha, 'dd/MM/yyyy')); //añadir fecha seleccionada a array de fechas
            fecha.setTime(fecha.getTime() - (60 * 60 * 24 * 7 * 1000)); //time actual en ms - (60s 60m 24h 7d milisegundos); resto una semana
        }
        this.fourweeks_year.lineChartLabels = fechas.reverse(); //ordenar labels
        var _loop_2 = function () {
            sem = this_2.sentinelawsdata.week - i;
            year = this_2.sentinelawsdata.year - 1; //datos del servicio de hace un año
            var /** @type {?} */ indi = indice.valueOf();
            if (sem <= 0) { //si semanas de año anterior
                year = year - 1; //retrocedo 1 año
                sem = 52 - sem; //retrocedo semanas desde la 52 (última de diciembre)
            }
            //crear el prefijo sentinel  (año, nºsemana)
            var /** @type {?} */ sentinelprefix = "processing_gc/" + this_2.sentinelawsdata.utmstr + "/" + year + "/" + sem + "/";
            //obtener índices NDVI con las coordenadas de parcela y el prefijo de fecha
            this_2.sentinel.getNDVIIndexes({ 'coords': ( /** @type {?} */(this_2.currentGeometry)).getCoordinates()[0],
                'sceneid': this_2.sentinel.sentinelurl.URL_BASE_NDVI_TILER + "/" + sentinelprefix, 'tilesize': 256 }, false).subscribe(function (response) {
                var /** @type {?} */ r = response.json();
                var /** @type {?} */ sat_date;
                if (r.satellite != undefined) {
                    _this.infosatellite[1][indi] = r.satellite;
                }
                if (r.indicesNDVI.length > 0) {
                    ndvidata[indi] = _this.sentinel.mediaNDVI(r); //r.indicesNDVI es un array que contiene los NDVI para esa localización y fecha
                }
                else {
                    ndvidata[indi] = null;
                }
                _this.fourweeks_year.lineChartData = [{ data: ndvidata, label: 'NDVI_' + _this.añoAnterior }];
                _this.fourweeks.lineChartData =
                    [{ data: _this.fourweeks.lineChartData[0].data, label: 'NDVI_' + _this.añoActual },
                        { data: ndvidata, label: 'NDVI_' + _this.añoAnterior }];
                if (ndvidata.length === 4) {
                    //this.loadingndvi = false;
                }
            });
            indice -= 1;
        };
        var this_2 = this, sem, year;
        for (var /** @type {?} */ i = 0; i < 4; i++) {
            _loop_2();
        }
    };
    /**
     * @param {?} year
     * @param {?} month
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.loadNDVI_month = function (year, month) {
        var _this = this;
        return new rxjs_Observable.Observable(function (observer) {
            var /** @type {?} */ ndvidata = [];
            var /** @type {?} */ indice;
            var /** @type {?} */ fecha = new Date();
            var /** @type {?} */ sem = 0;
            var /** @type {?} */ suma_mes = 0;
            var /** @type {?} */ media_mes = 0;
            fecha.setFullYear(year);
            fecha.setMonth(month);
            fecha.setDate(1);
            var /** @type {?} */ first_week = _this.sentinel.getWeekNumber(fecha);
            fecha.setMonth(month + 1); //primera semana del proximo mes (para llegar a la última semana del actual)
            var /** @type {?} */ last_week = _this.sentinel.getWeekNumber(fecha);
            if (first_week > last_week) { //caso de diciembre
                last_week = 52;
            }
            //Recorro desde primera semana del mes a la última
            for (var /** @type {?} */ semana = first_week; semana < last_week; semana++) {
                var /** @type {?} */ sem_1 = semana.valueOf();
                var /** @type {?} */ sentinelprefix = "processing_gc/" + _this.sentinelawsdata.utmstr + "/" + year + "/" + sem_1 + "/";
                _this.sentinel.getNDVIIndexes({ 'coords': ( /** @type {?} */(_this.currentGeometry)).getCoordinates()[0],
                    'sceneid': _this.sentinel.sentinelurl.URL_BASE_NDVI_TILER + "/" + sentinelprefix, 'tilesize': 256 }, false).subscribe(function (response) {
                    var /** @type {?} */ r = response.json();
                    if (r.indicesNDVI.length > 0) { //si han índices NDVI en esa parcela y esa fecha
                        ndvidata.push(_this.sentinel.mediaNDVI(r));
                    }
                    else {
                        ndvidata.push(null);
                    }
                    if (ndvidata.length === (last_week - first_week)) { //si he recorrido todas las semanas del mes
                        // this.loadingndvi = false; //stop evento de loading
                        ndvidata = ndvidata.filter(function (i) {
                            if (i !== null) {
                                console.log("Valor de i en lambda: " + i);
                                return i;
                            }
                        });
                        for (var /** @type {?} */ i = 0; i < ndvidata.length; i++) {
                            suma_mes += ndvidata[i];
                        }
                        media_mes = suma_mes / ndvidata.length;
                        var /** @type {?} */ resultado = { 'year': year, 'month': month, 'media': media_mes }; //JSON
                        observer.next(resultado); //devuelvo JSON
                        observer.complete(); //notificación completada
                    }
                });
            }
        });
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.loadNDVI_threemonths = function () {
        var _this = this;
        console.log("FECHA SELECCIONADA: ", this.fechaseleccionada);
        if (this.currentGeometry == undefined || this.currentGeometry == null) {
            return;
        }
        if (this.fechaseleccionada == undefined || this.fechaseleccionada == null) {
            //Obtener la fecha disponible más reciente:
            this.fechaseleccionada = this.date_latest;
            console.log("Fecha seleccionada por defecto (la más reciente): ", this.fechaseleccionada);
            this.compruebaEscena(this.fechaseleccionada);
        }
        this.loadingndvi = true; //evento de loading;
        var /** @type {?} */ media_meses = []; //datos de las medias de cada uno de los meses
        var /** @type {?} */ media_meses_year = []; //datos de las medias de cada uno de los meses de hace un año
        var /** @type {?} */ num_meses = 3;
        var /** @type {?} */ fecha = new Date(this.fechaseleccionada.getTime()); //fecha del datePicker;
        var /** @type {?} */ mesrequest = fecha.getMonth();
        var /** @type {?} */ yearequest = fecha.getFullYear();
        for (var /** @type {?} */ m = 0; m < num_meses; m++) {
            mesrequest = fecha.getMonth() - m;
            if (mesrequest < 0) { //si retrocedo a meses del año anterior
                yearequest = fecha.getFullYear() - 1; //retrocedo 1 año
                mesrequest = 12 + mesrequest; //retrocedo los meses correspondientes desde el último mes (diciembre==11; enero==0)
                //+ por ser mesrequest negativo
            }
            //LLAMADA A LA FUNCIÓN loadNDVI_month()
            this.loadNDVI_month(yearequest, mesrequest).subscribe(function (mediames) {
                media_meses.push(mediames);
                //Llamada a la función de mes hace un año
                _this.loadNDVI_month(mediames.year - 1, mediames.month).subscribe(function (mediamesprev) {
                    media_meses_year.push(mediamesprev);
                    if (media_meses.length === num_meses && media_meses_year.length === num_meses) {
                        var /** @type {?} */ mesesordenado = media_meses.sort(function (a, b) {
                            if (a.year < b.year) {
                                return -1;
                            }
                            else if (a.year === b.year) {
                                if (a.month <= b.month) {
                                    return -1;
                                }
                                else {
                                    return 1;
                                }
                            }
                            else {
                                return 1;
                            }
                        });
                        var /** @type {?} */ mesesordenadoyear = media_meses_year.sort(function (a, b) {
                            if (a.year < b.year) {
                                return -1;
                            }
                            else if (a.year === b.year) {
                                if (a.month <= b.month) {
                                    return -1;
                                }
                                else {
                                    return 1;
                                }
                            }
                            else {
                                return 1;
                            }
                        });
                        var /** @type {?} */ meses_final_1 = [];
                        var /** @type {?} */ meses_final_year_1 = [];
                        var /** @type {?} */ labels_meses_1 = [];
                        var /** @type {?} */ nombresMeses = ['Enero', 'Febrero', 'Marzo',
                            'Abril', 'Mayo', 'Junio',
                            'Julio', 'Agosto', 'Septiembre',
                            'Octubre', 'Noviembre', 'Diciembre'];
                        mesesordenado.forEach(function (element) {
                            meses_final_1.push(element.media);
                            labels_meses_1.push(nombresMeses[element.month]);
                        });
                        mesesordenadoyear.forEach(function (element) {
                            meses_final_year_1.push(element.media);
                        });
                        _this.threemonths.lineChartLabels = labels_meses_1;
                        /*el timeout se ha quitado*/
                        _this.loadingndvi = false;
                        console.log("Estoy en loadNDVI_threemonths()");
                        _this.threemonths.lineChartData =
                            [{ data: meses_final_1, label: 'NDVI_' + _this.añoActual },
                                { data: meses_final_year_1, label: 'NDVI_' + _this.añoAnterior }];
                        console.log("THREE_MONTHS_CHART: ");
                        console.log(_this.threemonths);
                    }
                });
            });
        }
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.loadNDVI_threemonths_year = function () {
        var _this = this;
        console.log("FECHA SELECCIONADA: ", this.fechaseleccionada);
        if (this.currentGeometry == undefined || this.currentGeometry == null) {
            return;
        }
        if (this.fechaseleccionada == undefined || this.fechaseleccionada == null) {
            //Obtener la fecha disponible más reciente:
            this.fechaseleccionada = this.date_latest;
            console.log("Fecha seleccionada por defecto (la más reciente): ", this.fechaseleccionada);
            this.compruebaEscena(this.fechaseleccionada);
        }
        this.loadingndvi = true; //evento de loading;
        var /** @type {?} */ media_meses = []; //datos de las medias de cada uno de los meses
        var /** @type {?} */ num_meses = 3;
        var /** @type {?} */ fecha = new Date(this.fechaseleccionada.getTime()); //fecha del datePicker;
        fecha.setFullYear(fecha.getFullYear() - 1); //Establecemos la fecha a hace un año
        var /** @type {?} */ mesrequest = fecha.getMonth();
        var /** @type {?} */ yearequest = fecha.getFullYear();
        for (var /** @type {?} */ m = 0; m < num_meses; m++) {
            mesrequest = fecha.getMonth() - m;
            if (mesrequest < 0) { //si retrocedo a meses del año anterior
                yearequest = fecha.getFullYear() - 1; //retrocedo 1 año
                mesrequest = 12 + mesrequest; //retrocedo los meses correspondientes desde el último mes (diciembre==11; enero==0)
                //+ por ser mesrequest negativo
            }
            //LLAMADA A LA FUNCIÓN loadNDVI_month()
            this.loadNDVI_month(yearequest, mesrequest).subscribe(function (mediames) {
                media_meses.push(mediames);
                if (media_meses.length === num_meses) {
                    var /** @type {?} */ mesesordenado = media_meses.sort(function (a, b) {
                        if (a.year < b.year) {
                            return -1;
                        }
                        else if (a.year === b.year) {
                            if (a.month <= b.month) {
                                return -1;
                            }
                            else {
                                return 1;
                            }
                        }
                        else {
                            return 1;
                        }
                    });
                    var /** @type {?} */ meses_final_2 = [];
                    var /** @type {?} */ labels_meses_2 = [];
                    var /** @type {?} */ nombresMeses = ['Enero', 'Febrero', 'Marzo',
                        'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre',
                        'Octubre', 'Noviembre', 'Diciembre'];
                    mesesordenado.forEach(function (element) {
                        meses_final_2.push(element.media);
                        labels_meses_2.push(nombresMeses[element.month]);
                    });
                    _this.threemonths_year.lineChartLabels = labels_meses_2;
                    /*el timeout se ha quitado*/
                    _this.loadingndvi = false;
                    console.log("Estoy en loadNDVI_threemonths_year()");
                    console.log("threemonths_year.lineChartData= ", _this.threemonths_year.lineChartData);
                    console.log("threemonths.lineChartData= ", _this.threemonths.lineChartData);
                    _this.threemonths_year.lineChartData = [{ data: meses_final_2, label: 'NDVI_' + _this.añoAnterior }];
                    _this.threemonths.lineChartData =
                        [{ data: _this.threemonths.lineChartData[0].data, label: 'NDVI_' + _this.añoActual },
                            { data: meses_final_2, label: 'NDVI_' + _this.añoAnterior }];
                }
            });
        }
    };
    /**
     * @param {?} e
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.chartClicked = function (e) {
        if (e.active.length > 0) {
            var /** @type {?} */ datasetIndex = e.active[0]._datasetIndex;
            var /** @type {?} */ dataIndex = e.active[0]._index;
            console.log("dataset " + datasetIndex);
            console.log("indice " + dataIndex);
            var /** @type {?} */ dataObject = this.infosatellite[datasetIndex][dataIndex];
            if (dataObject != undefined) {
                this.satInfoToShow = dataObject;
            }
            console.log(dataObject);
        }
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.cerrarinfosat = function () {
        this.satInfoToShow = null;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.calculateNDVI = function (data) {
        this.ndvidata = new NdviData();
        this.ndvidata.imagedata = data.imagedata;
        for (var /** @type {?} */ n = 0; n < data.indicesNDVI.length; n++) {
            this.ndvidata.addIndiceNDVI(data.indicesNDVI[n]);
        }
        this.loadingndvi = false;
        console.log(this.ndvidata);
    };
    /**
     * @param {?} evt
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.seleccionarEscena = function (evt) {
        console.log(evt);
    };
    /**
     * @param {?} evt
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.seleccionarFecha = function (evt) {
        var _this = this;
        console.log(evt.value.getDate().toString);
        this.fechaseleccionada = evt.value;
        var /** @type {?} */ extent$$1 = this.map.getView().calculateExtent(this.map.getSize());
        this.sentinel.checkScene(ol.extent.getBottomLeft(extent$$1), evt.value).toPromise().then(function (retorno) {
            if (retorno.result == "OK") {
                _this.sentinelawsdata = retorno;
                _this.sentinelawsdata.exists = true;
                _this.sentinelawsprefix = "processing_gc/" + retorno.utmstr + "/" + retorno.year + "/" + retorno.week + "/";
                _this.checkEscenaAndCrop();
                if (_this.currentsigpacid !== null) {
                    _this.loadNDVI();
                    _this.loadNDVI_year();
                    _this.loadNDVI_threemonths();
                    // this.loadNDVI_threemonths_year();
                }
            }
            else {
                _this.sentinelawsdata = { 'exists': false };
            }
            console.log(retorno);
        });
    };
    /**
     * @param {?} fecha
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.compruebaEscena = function (fecha) {
        var _this = this;
        var /** @type {?} */ extent$$1 = this.map.getView().calculateExtent(this.map.getSize());
        this.sentinel.checkScene(ol.extent.getBottomLeft(extent$$1), fecha).toPromise().then(function (retorno) {
            if (retorno.result == "OK") {
                _this.sentinelawsdata = retorno;
                _this.sentinelawsdata.exists = true;
                _this.sentinelawsprefix = "processing_gc/" + retorno.utmstr + "/" + retorno.year + "/" + retorno.week + "/";
                _this.checkEscenaAndCrop();
                if (_this.currentsigpacid !== null) { //LAS SIGUIENTES 3 LÍNEAS DE LLAMADAS A FUNCIONES CREO QUE NO SON NECESARIAS
                    _this.loadNDVI();
                    _this.loadNDVI_year();
                    _this.loadNDVI_threemonths();
                    //this.loadNDVI_threemonths_year();;
                }
            }
            else {
                _this.sentinelawsdata = { 'exists': false };
            }
            console.log(retorno);
        });
    };
    /**
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.getUrlSentinel = function () {
        if (this.sentinelfunction === "ndvi" && this.sentinelawsprefix != null) {
            return (this.sentinel.sentinelurl.URL_BASE_NDVI_TILER + "/" +
                this.sentinelawsprefix +
                "{z}/{x}/{y}.png");
        }
    };
    /**
     * @param {?} pid
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.localizarParcela = function (pid) {
        var _this = this;
        this.loadingtiles = true;
        //Clear datepicker
        this.fechadate = new _angular_forms.FormControl(new Date().toISOString());
        //Clear Parcela SIGPAC e Indice NDVI
        this.currentsigpacid = null;
        this.currentndviindex = null;
        //Clear parcela
        this.map.getInteractions().dispatchEvent('deselect');
        this.select.getFeatures().clear();
        if (pid.split(',').length > 2) {
            var /** @type {?} */ params = pid.replace(/,/g, '/');
            this.service.getParcela(params).then(function (parcela) {
                var /** @type {?} */ puntos = new Array();
                if (parcela != null) {
                    for (var /** @type {?} */ i = 0; i < parcela.polygon[0].length; i++) {
                        var /** @type {?} */ coord = ol.proj.transform([parcela.polygon[0][i][0], parcela.polygon[0][i][1]], 'EPSG:3857', 'EPSG:4326');
                        puntos.push(coord);
                    }
                    var /** @type {?} */ multipoint = new ol.geom.LineString(puntos);
                    var /** @type {?} */ multis = /** @type {?} */ (multipoint.simplify(0.00002));
                    //Quito todas los recintos anteriores
                    _this.source.clear();
                    var /** @type {?} */ poly2 = new ol.geom.Polygon([multis.getCoordinates()]);
                    _this.map.getView().fit(poly2.getExtent());
                    _this.map.getView().setZoom(16);
                    //  this.map.getView().fitExtent(poly2.getExtent, this.map.getSize());
                    _this.singlefeature = true;
                    //this.cargarParcelarioSIGPAC();
                    var /** @type {?} */ feature = new ol.Feature({
                        name: parcela.id,
                        geometry: poly2,
                        atributos: parcela.atributos
                    });
                    _this.source.addFeature(feature);
                    _this.onSelectParcela(feature);
                    _this.loadingtiles = false;
                }
                else {
                    _this.loadingtiles = false;
                    _this.source.clear();
                    var /** @type {?} */ snackbarref = _this.snackBar.open('Recinto no encontrado', 'OK', { duration: 1000 });
                }
            });
        }
        else {
            //Se supone que es provincia municipio
            this.service.queryLocationByAddress(pid).then(function (locat) {
                _this.singlefeature = false;
                _this.map.setView(new ol.View({
                    projection: 'EPSG:4326',
                    center: [locat.lng, locat.lat],
                    zoom: 16
                }));
                _this.loadingtiles = false;
                _this.cargarParcelarioSIGPAC();
            });
        }
    };
    /**
     * @param {?} evento
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.mueveScroll = function (evento) {
        console.log("El valor en movimiento es " + evento.value);
        this.slider_valueDate = this.dates_sorted[evento.value - 1];
        console.log("El valor es " + this.slider_valueDate);
    };
    /**
     * @param {?} evento
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.cambiaScroll = function (evento) {
        var _this = this;
        this.loadingndvi = true;
        console.log("El valor definitivo es " + evento.value);
        this.slider_valueID = evento.value;
        var /** @type {?} */ fulldate = this.fulldates[evento.value - 1];
        console.log(fulldate);
        /*Calculo el año de la fecha obtenida*/
        var /** @type {?} */ year = fulldate.getFullYear();
        console.log(year);
        /*Calculo la semana del año de la fecha obtenida*/
        var /** @type {?} */ sem = this.sentinel.getWeekNumber(fulldate);
        console.log(sem);
        //crear el prefijo sentinel  (año, nºsemana)
        var /** @type {?} */ sentinelprefix = "processing_gc/" + this.sentinelawsdata.utmstr + "/" + year + "/" + sem + "/";
        //obtener índices NDVI con las coordenadas de parcela y el prefijo de fecha
        this.sentinel.getNDVIIndexes({ 'coords': ( /** @type {?} */(this.currentGeometry)).getCoordinates()[0],
            'sceneid': this.sentinel.sentinelurl.URL_BASE_NDVI_TILER + "/" + sentinelprefix, 'tilesize': 512 }, true).subscribe(function (response) {
            var /** @type {?} */ r = response.json();
            console.log("RESPONSE: ", r);
            _this.detailedimage = new Image();
            _this.imagedata = r.imagedata;
            _this.currentndetaildviindex = _this.sentinel.mediaNDVI(r);
            
            _this.detailedimage.src = _this.imagedata;
            _this.drawImageOnCanvas();
            _this.loadingndvi = false;
        });
    };
    /**
     * @param {?} value
     * @return {?}
     */
    VisorTeledeteccionComponent.prototype.formatLabel = function (value) {
        if (!value) {
            return 0;
        }
        if (value >= 100) {
            return Math.round(value / 1000) + 'k';
        }
        return value;
    };
    VisorTeledeteccionComponent.URL_TILER_BASE = "https://x9b58p1aed.execute-api.eu-west-1.amazonaws.com/production/sentinel/";
    VisorTeledeteccionComponent.decorators = [
        { type: _angular_core.Component, args: [{
                    selector: 'app-visorteledeteccion',
                    template: " <div class=\"panel panel-default\"> <div *ngIf=\"!configuracion.mobile_layout\" class=\"panel-body\"> <div class=\"row\"> <div class=\"col col-md-4\"> <ngx-loading [show]=\"loadingndvi\" ></ngx-loading> <mat-form-field class=\"full-width\"> <input matInput [matDatepickerFilter]=\"filtrarfechasatelites\" [matDatepicker]=\"selfecha\" placeholder=\"Seleccione una fecha\" (dateChange)=\"seleccionarFecha($event)\" [formControl]=\"fechadate\"> <mat-datepicker-toggle matSuffix [for]=\"selfecha\"></mat-datepicker-toggle> <mat-datepicker #selfecha ></mat-datepicker> </mat-form-field> <mat-card class=\"full-width\" *ngIf=\"currentsigpacid && satInfoToShow\"> <mat-card-header > <div mat-card-avatar class=\"satellite-image\"></div> <mat-card-title>Satelite SENTINEL</mat-card-title> <mat-card-subtitle class=\"header-subtitle\">Información detallada de la muestra</mat-card-subtitle> </mat-card-header> <mat-card-content> <ul class=\"list-group\"> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">Fecha de vuelo: </span> <br/><p class=\" label-value-sat\">{{satInfoToShow.timestamp}}</p></li> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">% Nublado - % Cobertura: </span> <br/><p class=\" label-value-sat\">{{satInfoToShow.cloudyPixelPercentage}} - {{satInfoToShow.dataCoveragePercentage}}</p></li> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">Zona UTM - Banda lat. - Cuadrado GRID: </span> <br/><p class=\" label-value-sat\">{{satInfoToShow.utmZone}} - {{satInfoToShow.latitudeBand}} - {{satInfoToShow.gridSquare}}</p></li> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">Nombre producto: </span> <br/><p class=\"label-value-sat\">{{satInfoToShow.productName}}</p></li> </ul> <button mat-raised-button class=\"full-width\" color=\"primary\" (click)=\"cerrarinfosat()\">Cerrar</button> </mat-card-content> </mat-card> <mat-card class=\"full-width\" *ngIf=\"currentsigpacid && !satInfoToShow\"> <mat-card-header *ngIf=\"sentinelawsdata\"> <div mat-card-avatar class=\"satellite-image\"></div> <mat-card-title *ngIf=\"currentsigpacid\">Parcela SIGPAC: {{currentsigpacid}}</mat-card-title> <mat-card-subtitle class=\"header-subtitle\" *ngIf=\"currentsigpacid\">Indice NDVI: {{currentndviindex}}</mat-card-subtitle> </mat-card-header> <hr> <!--Primera gráfica para visualizar las últimas cuatro semanas--> <mat-card-content> <p align=\"center\"><b>Últimas 4 semanas</b></p> <canvas *ngIf=\"fourweeks.lineChartData\" baseChart width=\"100%\" [datasets]=\"fourweeks.lineChartData\" [labels]=\"fourweeks.lineChartLabels\" [colors]=\"fourweeks.lineChartColors\" [options]=\"fourweeks.lineChartOptions\" [legend]=\"'true'\" chartType=\"bar\" (chartClick)=\"chartClicked($event)\"></canvas> </mat-card-content> <hr> <!--Segunda gráfica para visualizar las últimas cuatro semanas de hace un año--> <!-- <mat-card-content> <p align=\"center\"><b>Últimas 4 semanas hace 1 año</b></p> <canvas *ngIf=\"fourweeks_year.lineChartData\" baseChart width=\"100%\" [datasets]=\"fourweeks_year.lineChartData\" [labels]=\"fourweeks_year.lineChartLabels\" [colors]=\"fourweeks_year.lineChartColors\" [options]=\"fourweeks_year.lineChartOptions\" [legend]=\"'false'\" chartType=\"line\"></canvas> </mat-card-content> <hr> --> <!--Tercera gráfica (de barras) para visualizar los últimos 3 meses--> <mat-card-content> <p align=\"center\"><b>Últimos 3 meses</b></p> <canvas *ngIf=\"threemonths.lineChartData\" baseChart width=\"100%\" [datasets]=\"threemonths.lineChartData\" [labels]=\"threemonths.lineChartLabels\" [colors]=\"threemonths.lineChartColors\" [options]=\"threemonths.lineChartOptions\" [legend]=\"'false'\" chartType=\"bar\"></canvas> </mat-card-content> </mat-card> </div> <div class=\"col col-md-8\"> <ngx-loading [show]=\"loadingtiles\"></ngx-loading> <div class=\"map-container\" [attr.id]=\"config.target\"></div> </div> </div> </div> <!--LAYOUT MOBILE--> <div *ngIf=\"configuracion.mobile_layout\" class=\"panel-body\"> <div class=\"row\"> <div class=\"col col-md-12\"> <mat-button-toggle-group class=\"full-width\" name=\"teledeteccionpanel\"  [(ngModel)]=\"grupopanel\"> <mat-button-toggle class=\"full-width\" checked=\"true\" value=\"mapa\" (click)=\"refreshMapa()\">Ver Mapa</mat-button-toggle> <mat-button-toggle class=\"full-width\" value=\"indices\">Ver indices</mat-button-toggle> <mat-button-toggle class=\"full-width\" value=\"detalle\" (click)=\"drawImageOnCanvas()\">Detallado</mat-button-toggle> </mat-button-toggle-group> </div> </div> <div [hidden]=\"grupopanel != 'mapa'\"  class=\"row\"> <div class=\"col col-md-12\"> <div class=\"row margin-superior\"> <div class=\"col col-md-12\"> <mat-form-field class=\"full-width mt-10\"> <input matInput [matDatepickerFilter]=\"filtrarfechasatelites\" [matDatepicker]=\"selfecha\" placeholder=\"Seleccione una fecha\" (dateChange)=\"seleccionarFecha($event)\" [formControl]=\"fechadate\"> <mat-datepicker-toggle matSuffix [for]=\"selfecha\"></mat-datepicker-toggle> <mat-datepicker #selfecha ></mat-datepicker> </mat-form-field> </div> </div> <ngx-loading [show]=\"loadingtiles\"></ngx-loading> <div class=\"map-container-mobile\" [attr.id]=\"config.target\"></div> </div> </div> <div [hidden]=\"grupopanel != 'indices'\" class=\"row\"> <div class=\"col col-md-12\"> <ngx-loading [show]=\"loadingndvi\" ></ngx-loading> <mat-card class=\"full-width\" *ngIf=\"currentsigpacid && satInfoToShow\"> <mat-card-header > <div mat-card-avatar class=\"satellite-image\"></div> <mat-card-title>Satelite SENTINEL</mat-card-title> <mat-card-subtitle class=\"header-subtitle\">Información detallada de la muestra</mat-card-subtitle> </mat-card-header> <mat-card-content> <ul class=\"list-group\"> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">Fecha de vuelo: </span> <br/><p class=\" label-value-sat\">{{satInfoToShow.timestamp}}</p></li> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">% Nublado - % Cobertura: </span> <br/><p class=\" label-value-sat\">{{satInfoToShow.cloudyPixelPercentage}} - {{satInfoToShow.dataCoveragePercentage}}</p></li> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">Zona UTM - Banda lat. - Cuadrado GRID: </span> <br/><p class=\" label-value-sat\">{{satInfoToShow.utmZone}} - {{satInfoToShow.latitudeBand}} - {{satInfoToShow.gridSquare}}</p></li> <li class=\"list-group-item\"><span class=\"label label-default label-sat\">Nombre producto: </span> <br/><p class=\"label-value-sat\">{{satInfoToShow.productName}}</p></li> </ul> <button mat-raised-button class=\"full-width\" color=\"primary\" (click)=\"cerrarinfosat()\">Cerrar</button> </mat-card-content> </mat-card> <mat-card class=\"full-width\" *ngIf=\"currentsigpacid && !satInfoToShow\"> <mat-card-header *ngIf=\"sentinelawsdata\"> <div mat-card-avatar class=\"satellite-image\"></div> <mat-card-title *ngIf=\"currentsigpacid\">Parcela SIGPAC: {{currentsigpacid}}</mat-card-title> <mat-card-subtitle class=\"header-subtitle\" *ngIf=\"currentsigpacid\">Indice NDVI: {{currentndviindex}}</mat-card-subtitle> </mat-card-header> <hr> <!--Primera gráfica para visualizar las últimas cuatro semanas--> <mat-card-content> <p align=\"center\"><b>Últimas 4 semanas</b></p> <canvas *ngIf=\"fourweeks.lineChartData\" baseChart width=\"100%\" [datasets]=\"fourweeks.lineChartData\" [labels]=\"fourweeks.lineChartLabels\" [colors]=\"fourweeks.lineChartColors\" [options]=\"fourweeks.lineChartOptions\" [legend]=\"'true'\" chartType=\"bar\" (chartClick)=\"chartClicked($event)\"></canvas> </mat-card-content> <hr> <!--Segunda gráfica para visualizar las últimas cuatro semanas de hace un año--> <!-- <mat-card-content> <p align=\"center\"><b>Últimas 4 semanas hace 1 año</b></p> <canvas *ngIf=\"fourweeks_year.lineChartData\" baseChart width=\"100%\" [datasets]=\"fourweeks_year.lineChartData\" [labels]=\"fourweeks_year.lineChartLabels\" [colors]=\"fourweeks_year.lineChartColors\" [options]=\"fourweeks_year.lineChartOptions\" [legend]=\"'false'\" chartType=\"line\"></canvas> </mat-card-content> <hr> --> <!--Tercera gráfica (de barras) para visualizar los últimos 3 meses--> <mat-card-content> <p align=\"center\"><b>Últimos 3 meses</b></p> <canvas *ngIf=\"threemonths.lineChartData\" baseChart width=\"100%\" [datasets]=\"threemonths.lineChartData\" [labels]=\"threemonths.lineChartLabels\" [colors]=\"threemonths.lineChartColors\" [options]=\"threemonths.lineChartOptions\" [legend]=\"'false'\" chartType=\"bar\"></canvas> </mat-card-content> </mat-card> </div> </div> <div [hidden]=\"grupopanel != 'detalle'\"  class=\"row\"> <div class=\"col col-md-12\"> <ngx-loading [show]=\"loadingndvi\" ></ngx-loading> <mat-card class=\"full-width\" *ngIf=\"currentsigpacid\"> <mat-card-header *ngIf=\"sentinelawsdata\"> <div mat-card-avatar><mat-icon mat-list-icon>blur_on</mat-icon></div> <mat-card-title *ngIf=\"currentsigpacid\">Detalle Parcela SIGPAC: {{currentsigpacid}}</mat-card-title> <mat-card-subtitle class=\"header-subtitle\" *ngIf=\"currentsigpacid\">Indice NDVI: {{currentndetaildviindex}}</mat-card-subtitle> </mat-card-header> <hr> <mat-card-content style=\"text-align: center\"> <p [hidden]=\"pixelndvidata\" align=\"center\">Pinche en la imagen para obtener indice NDVI</p> <div *ngIf=\"pixelndvidata && pixelndvidata.ndvi\" class=\"canvasinfo\"> <span  class=\"ndvilegend\" [style.background-color]=\"pixelndvidata.color\"></span> <span  class=\"ndvivalue\">Indice seleccionado: {{pixelndvidata.ndvi | number:'1.2-2'}}</span> </div> <div #canvascontainer class=\"canvascontainer\"> <canvas class=\"canvasparcela\" #parceladetalle (click)=\"picarPixel($event)\"> </canvas> <div *ngIf=\"pixelndvidata\" [style.top]=\"(pixelndvidata.loc.y-8)+'px'\" [style.left]=\"(pixelndvidata.loc.x-8)+'px'\" #selectorpixel class=\"gun_pointer\"></div> </div> <hr> <mat-card> <mat-card-content> <p>Seleccionar fechas disponibles NDVI</p> <section class=\"example-section\"> <p>{{slider_valueDate}}</p> </section> <!--Primer slider gestionado a través de @Input() set slider_valueID(value: number)--> <!--<mat-slider class=\"example-margin\" [disabled]=\"false\" [invert]=\"false\" [min]=\"1\" [max]=\"num_items_slider\" [step]=\"1\" [thumbLabel]=\"true\" [tickInterval]=\"auto\" [(ngModel)]=\"slider_valueID\" [vertical]=\"false\"> </mat-slider>--> <!--Segundo slider para probar eventos EventEmitter--> <mat-slider [min]=\"1\" [max]=\"num_items_slider\" [step]=\"1\" [thumbLabel]=\"false\" [tickInterval]=\"auto\" [value]=\"num_items_slider\" (input)=\"mueveScroll($event)\" (change)=\"cambiaScroll($event)\" > <!--[change]=\"change\" [input]=\"input\"--> </mat-slider> </mat-card-content> </mat-card> </mat-card-content> </mat-card> </div> </div> </div> </div> ",
                    styles: [" .map-container{ width:100%; height:600px; } .map-container-mobile{ width:100%; height:300px; } .list-height{ min-height:600px; overflow-y: scroll; overflow-x: hidden; width:100%; } .full-width,.full-width /deep/ .mat-button-toggle-label,.full-width /deep/ .mat-button-toggle-label-content{ width:100%; } .scene_date{ font-size: 16px; line-height: 20px; font-weight: bold; } .scene_cloud{ font-size: 12px; line-height:16px; margin-left:20px; } .icono_scene{ font-size:14px; width:14px; } .osmvisor-radio-group { display: inline-flex; flex-direction: row; } .osmvisor-radio-button { margin: 5px; } .osmvisor-selected-value { margin: 15px 0; } .sceneitem{ height:auto!important; } .satellite-image { background-image: url('/assets/space-satellite.png'); background-size: cover; } .satellite-image-red { background-image: url('/assets/space-satellite-red.png'); background-size: cover; } .header-subtitle{ color: #94b369; font-weight: bold; } .label-sat{ font-size:14px; font-weight:normal; } .label-value-sat{ font-size:14px; font-weight:normal; margin-top:10px; margin-left:20px; display:block; text-align:left; word-break: break-all; } .margin-superior{ margin-top:12px; } .canvasparcela,.canvascontainer,.canvasinfo{ width:100%; max-width:400px; margin-left:auto; margin-right:auto; position:relative; } .ndvilegend{ width:20px; height:20px; display: inline-block; } .gun_pointer{ width:16px; height:16px; position:absolute; background:url('/assets/gun-pointer.png') } .mat-slider-horizontal { width: 100%; }"],
                    providers: [FegarequestsService]
                },] },
    ];
    /**
     * @nocollapse
     */
    VisorTeledeteccionComponent.ctorParameters = function () { return [
        { type: FegarequestsService, },
        { type: SentinelDataService, },
        { type: _angular_core.NgZone, },
        { type: _angular_material.MatSnackBar, },
        { type: _angular_common.DatePipe, },
        { type: _angular_material.DateAdapter, },
    ]; };
    VisorTeledeteccionComponent.propDecorators = {
        'canvasparcela': [{ type: _angular_core.ViewChild, args: ['parceladetalle',] },],
        'canvascontainer': [{ type: _angular_core.ViewChild, args: ['canvascontainer',] },],
        'selectorpixel': [{ type: _angular_core.ViewChild, args: ['selectorpixel',] },],
        'centermap': [{ type: _angular_core.Input },],
        'recintos': [{ type: _angular_core.Input },],
        'configuracion': [{ type: _angular_core.Input },],
        'seleccionada': [{ type: _angular_core.Output },],
        'change': [{ type: _angular_core.Output },],
        'input': [{ type: _angular_core.Output },],
    };
    return VisorTeledeteccionComponent;
}());

var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EsDateAdapter = /** @class */ (function (_super) {
    __extends(EsDateAdapter, _super);
    function EsDateAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @return {?}
     */
    EsDateAdapter.prototype.getFirstDayOfWeek = function () {
        return 1;
    };
    return EsDateAdapter;
}(_angular_material.NativeDateAdapter));

var TeledeteccionModule = /** @class */ (function () {
    function TeledeteccionModule() {
    }
    /**
     * @param {?} sentinelurls
     * @return {?}
     */
    TeledeteccionModule.forRoot = function (sentinelurls) {
        return { ngModule: TeledeteccionModule,
            providers: [SentinelDataService, sentinelurls] };
    };
    TeledeteccionModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    imports: [
                        _angular_forms.FormsModule, _angular_forms.ReactiveFormsModule,
                        _angular_http.HttpModule,
                        _angular_common.CommonModule,
                        _angular_material.MatListModule,
                        _angular_material.MatButtonModule,
                        _angular_material.MatRadioModule,
                        _angular_material.MatIconModule,
                        _angular_material.MatInputModule,
                        _angular_material.MatDatepickerModule,
                        _angular_material.MatCardModule,
                        ngxLoading.LoadingModule,
                        _angular_platformBrowser_animations.BrowserAnimationsModule,
                        _angular_material.MatButtonToggleModule,
                        _angular_material.MatNativeDateModule,
                        _angular_material.MatSnackBarModule,
                        ng2Charts.ChartsModule,
                        _angular_material_slider.MatSliderModule,
                    ],
                    declarations: [VisorTeledeteccionComponent],
                    providers: [_angular_common.DatePipe, {
                            provide: _angular_material.DateAdapter, useClass: EsDateAdapter
                        },
                        SentinelDataService],
                    exports: [VisorTeledeteccionComponent, _angular_material.MatListModule, _angular_material.MatRadioModule]
                },] },
    ];
    /**
     * @nocollapse
     */
    TeledeteccionModule.ctorParameters = function () { return []; };
    return TeledeteccionModule;
}());

/**
 * Generated bundle index. Do not edit.
 */

exports.OsmeditorComponent = OsmeditorComponent;
exports.OsmvisorComponent = OsmvisorComponent;
exports.OsmeditorModule = OsmeditorModule;
exports.VisorTeledeteccionComponent = VisorTeledeteccionComponent;
exports.TeledeteccionModule = TeledeteccionModule;
exports.ParcelaModificada = ParcelaModificada;
exports.Parcela = Parcela;
exports.ɵa = FegarequestsService;
exports.ɵb = SentinelDataService;
exports.ɵc = EsDateAdapter;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=globalcaja-angular-lib.umd.js.map

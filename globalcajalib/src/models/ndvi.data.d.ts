export declare class NdviData {
    negativo: number;
    muy_bajo: number;
    bajo: number;
    normal: number;
    alto: number;
    muy_alto: number;
    total: number;
    suma_indices: number;
    imagedata: string;
    constructor();
    getPorcentaje(tipo: string): number;
    incrementoUnidad(tipo: string): void;
    addIndiceNDVI(indice: number): void;
}

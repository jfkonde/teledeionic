export declare class SentinelScene {
    path: string;
    num: string;
    acquisition_date: string;
    browseURL: string;
    scene_id: string;
    cloud_coverage: number;
    coverage: number;
}

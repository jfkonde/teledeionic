export declare class Base64Binary {
    static keyStr: string;
    static decodeArrayBuffer(b: any): any;
    static decode(b: any, a: any): any;
}

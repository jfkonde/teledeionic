import { Parcela } from './parcela.model';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
export declare class FegarequestsService {
    private http;
    private static readonly URL_BASE;
    private static readonly URL_BASE_QUERY;
    constructor(http: Http);
    getParcelas(coords1: any, coords2: any, recintos: boolean): Promise<Array<Parcela>>;
    queryLocationByAddress(ciudad: string): Promise<any>;
    getExtendedAtributos(atributos: any, recinto: boolean): Promise<any>;
    getParcela(params: string): Promise<Parcela>;
}

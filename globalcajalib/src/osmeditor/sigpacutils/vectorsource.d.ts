import { GeoTileBounds } from './geotilebounds';
export declare class VectorSource {
    static getURLTiles(b: GeoTileBounds, srid: number): string[];
}

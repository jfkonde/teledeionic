import { GeoTileSystemTileIndex } from './geotilesystemtileindex';
export declare class GeoTileSystem {
    pars: any;
    epsilon: number;
    nx0: number;
    ny0: number;
    srid: any;
    constructor(a: any);
    private GetSridParams(a);
    private LoadPixelsPerVectorUnit(b);
    XYToTileIJ(a: any, b: any, c: any): GeoTileSystemTileIndex;
}

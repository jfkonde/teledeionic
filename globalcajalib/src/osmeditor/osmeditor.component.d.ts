import { ParcelaModificada } from './parcelamodificada.model';
import { FegarequestsService } from './sigpacutils/fegarequests.service';
import { OnInit, EventEmitter, SimpleChanges } from '@angular/core';
import * as ol from 'openlayers';
export declare class OsmeditorComponent implements OnInit {
    private service;
    map: ol.Map;
    feature: ol.Feature;
    featureoriginal: ol.Feature;
    source: ol.source.Vector;
    source2: ol.source.Vector;
    centermap: string;
    config: any;
    trackonchange: boolean;
    modificada: EventEmitter<ParcelaModificada>;
    editando: boolean;
    coordinates: [ol.Coordinate];
    constructor(service: FegarequestsService);
    editarParcelaAgraria(parcela: any): void;
    editarCoordenadasParcela(parcela: any, coordenadasparcela: number[][]): void;
    ngOnInit(): void;
    initMap(): void;
    ngAfterViewInit(): any;
    ngOnChanges(cambios: SimpleChanges): void;
    aceptarPolygon(): void;
}

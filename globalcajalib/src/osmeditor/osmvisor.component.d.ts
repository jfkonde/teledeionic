import { FegarequestsService } from './sigpacutils/fegarequests.service';
import { Parcela } from './sigpacutils/parcela.model';
import { OnInit, EventEmitter, SimpleChanges } from '@angular/core';
import * as ol from 'openlayers';
export declare class OsmvisorComponent implements OnInit {
    private service;
    map: ol.Map;
    feature: ol.Feature;
    featureoriginal: ol.Feature;
    source: ol.source.Vector;
    parcelascargadas: Array<Parcela>;
    parcelasselected: any;
    config: any;
    centermap: string;
    recintos: boolean;
    seleccionada: EventEmitter<any>;
    constructor(service: FegarequestsService);
    ngOnInit(): void;
    ngAfterViewInit(): any;
    initMap(): void;
    buscarlocalidad(ciudad: string): void;
    ngOnChanges(changes: SimpleChanges): void;
    onSelectParcela(evt: any): void;
    onMoveMapa(): void;
}

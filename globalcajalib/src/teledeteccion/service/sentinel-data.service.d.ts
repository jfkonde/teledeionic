import { SentinelScene } from './../../models/sentinel.scene';
import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';
/**
 * SentinelDataService class.
 */
export declare class SentinelDataService {
    private httpclient;
    sentinelurl: any;
    cacheSentinelScenes: Observable<SentinelScene[]>;
    cacheSentinelUrlrequest: string;
    constructor(httpclient: Http, sentinelurl: any);
    checkScene(ll: number[], fecha: Date): Observable<any>;
    disponibilidad(utmstr: string): Observable<Date[]>;
    getListScene(ll: number[]): Observable<SentinelScene[]>;
    getNDVIIndexes(request: any, withimage: boolean): Observable<any>;
    convertToMGRS(ll: number[]): string;
    getDatesFromWeekNumber(year: number, week: number): Date[];
    getWeekNumber(d: Date): number;
    mediaNDVI(data: any): number;
    getNDVIPixelIndes(rgba: Uint8ClampedArray): number;
    nearestNDVIValue(rgba: Uint8ClampedArray): number;
}

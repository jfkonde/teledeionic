import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { VisoresPage } from '../pages/visores/visores';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TeledeteccionModule } from 'globalcaja-angular-lib';
import { DateAdapter, MatButtonModule, MatButtonToggleModule, MatCardModule, MatDatepickerModule, MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatRadioModule, MatSnackBar, MatSnackBarModule, NativeDateAdapter } from '@angular/material';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
    'rotate': { enable: true } //rotate is disabled by default, so we need to enable it
  }
}
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    VisoresPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    MatInputModule,
    TeledeteccionModule.forRoot(
      { provide: 'SENTINEL_URLS', useValue: {
        URL_BASE_DATA : "https://ya32ayry19.execute-api.eu-west-1.amazonaws.com/production/sentinel",
        URL_BASE_NDVI_TILER :"https://ge2294g24e.execute-api.eu-west-1.amazonaws.com/production/sentinel",
        URL_BADE_NDVI :"https://ya32ayry19.execute-api.eu-west-1.amazonaws.com/production"

    }}
  )
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    VisoresPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, {
    provide: HAMMER_GESTURE_CONFIG,
    useClass: CustomHammerConfig
  }
  ]
})
export class AppModule {}

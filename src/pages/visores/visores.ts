import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';
/**
 * Generated class for the VisoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visores',
  templateUrl: 'visores.html',
})
export class VisoresPage {
  teleconfig = { mobile_layout: true };
  title = "app";
  verrecintos: boolean ;
  sigpacid: string;
  today
  private todo : FormGroup;


  public yearSelected= localStorage.getItem('CodEmpresa');
  public listacultivos = [];

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
      this.today = new Date().toISOString();
      this.todo = this.formBuilder.group({

    });
  }
  ionViewWillEnter() {

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad VisoresPage');
  }


  logForm(){
     console.log(this.todo)
   }
verfecha(val){
  console.log(val);
}
seleccionarparcela(val){
  console.log(val);
}

}

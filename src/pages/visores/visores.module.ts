import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisoresPage } from './visores';
import { TeledeteccionModule } from 'globalcaja-angular-lib';
import { DateAdapter, MatButtonModule, MatButtonToggleModule, MatCardModule, MatDatepickerModule, MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatRadioModule, MatSnackBar, MatSnackBarModule, NativeDateAdapter } from '@angular/material';


@NgModule({
  declarations: [
    VisoresPage,
  ],
  imports: [
    IonicPageModule.forChild(VisoresPage),
    MatDatepickerModule,
    TeledeteccionModule.forRoot(
      { provide: 'SENTINEL_URLS', useValue: {
        URL_BASE_DATA : "https://ya32ayry19.execute-api.eu-west-1.amazonaws.com/production/sentinel",
        URL_BASE_NDVI_TILER :"https://ge2294g24e.execute-api.eu-west-1.amazonaws.com/production/sentinel",
        URL_BADE_NDVI :"https://ya32ayry19.execute-api.eu-west-1.amazonaws.com/production"

    }}
  )
  ],
})
export class VisoresPageModule {}
